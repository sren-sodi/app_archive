FROM php:7.3-apache

RUN apt-get update && apt-get install -y libpq-dev && docker-php-ext-install pdo pdo_pgsql && a2enmod headers rewrite proxy_http
RUN echo "Listen 8090" >> /etc/apache2/ports.conf
