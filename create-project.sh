#!/bin/bash

sudo apt update \
    && sudo apt install -yqq php apache2 libapache2-mod-php php-pgsql \
    && sudo service apache2 restart \
    && sudo apt install -yqq zip curl php-json php-xml php-pdo php-pgsql

# creation du projet
composer --no-interaction create-project symfony/skeleton $1-back 4.*

# ajout des packages courants dans une application REST Symfony
composer require --working-dir=$1-back --no-interaction --sort-packages \
    jms/serializer-bundle \
    friendsofsymfony/rest-bundle \
    symfony/orm-pack \
    symfony/security-bundle \
    symfony/workflow \
    symfony/validator \
    symfony/form \
    nelmio/api-doc-bundle \
    symfony/asset \
    symfony/twig-bundle \
    jms/serializer-bundle \
    lexik/jwt-authentication-bundle 

# ajout des packages courants pour développer une application Symfony
composer require --working-dir=$1-back --no-interaction --dev --sort-packages \
    symfony/requirements-checker \
    symfony/maker-bundle \
    symfony/phpunit-bridge \
    behat/behat \
    doctrine/doctrine-fixtures-bundle \
    behat/symfony2-extension

# lancement du serveur natif php
#php -S localhost:8080 -t $1-back/public &

curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt install -yqq nodejs && sudo npm install -g @angular/cli
ng new $1-front
#ng serve --open $1-front
cd $1-front
npm install primeng --save
npm install primeicons --save