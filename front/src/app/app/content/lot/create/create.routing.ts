import { Route } from '@angular/router';
import { CreateComponent } from './create.component';
import { CreateLotResolver } from '../shared/create-lot.resolver';

export const createLotRouting: Route =
{
    path: 'create', component: CreateComponent,
    resolve:{
        lot: CreateLotResolver
    }
}; 