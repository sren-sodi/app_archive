import { Component, OnInit } from '@angular/core';
import { LotForm } from '../shared/lot.form';
import { LotService } from '../shared/lot.service';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-lot-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  lotForm: LotForm;
  nbFiles: number;
  loadingFileNumber: Subscription;

  constructor(
    private lotService: LotService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    private messageService: MessageService
  ) { }

  isGettingFileNumber(): boolean {
    return false === this.loadingFileNumber.closed;
  }

  onSubmit() {
    this.lotService.submit(this.lotForm.value).subscribe(
      values => {
        this.messageService.add({ severity: 'info', summary: 'Le lot ' + this.lotForm.get('number').value + " a été créé.", detail: '' });
        this.router.navigate(['..'], {
          relativeTo: this.activedRoute
        });
      }
    );
  }

  ngOnInit() {
    this.nbFiles = null;
    this.loadingFileNumber = new Subscription();
    this.lotForm = new LotForm();
    this.activedRoute.data.subscribe(data => {
      console.log(data);
      this.lotForm.setValue(data.lot) ;});
    this.lotForm.valueChanges.pipe(debounceTime(500)).subscribe(lot => {
      if (this.lotForm.valid) {
        this.loadingFileNumber = this.lotService.estimateLot(lot).subscribe(number => {
          this.nbFiles = number;
        })
      }
    });
  }
}
