import { Component, OnInit } from '@angular/core';
import { Page } from './shared/page';
import { ActivatedRoute, Router } from '@angular/router';
import { Lot } from 'src/app/app/content/lot/shared/lot';
import { PageForm } from './shared/page-form';
import { File } from '../../shared/file';
import { PageService } from './shared/page-service';
import { FormControl } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  public page: Page;
  public lot: Lot;
  public pageForm: PageForm;

  pageInputNumber: string = "";

  constructor(
    private activedRoute: ActivatedRoute,
    private router: Router,
    private pageService: PageService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.activedRoute.data.subscribe(data => {
      this.pageForm = new PageForm(data.page);

      this.page = data.page;
    });
    this.activedRoute.parent.data.subscribe(data => {
      this.lot = data.lot;
    });
  }

  getFile(index: number): File {
    return this.page.files[index];

  }
  trackByFn(index, row) {
    return index;
  }

  next() {
    this.router.navigate(['..', this.page.number + 1], {
      relativeTo: this.activedRoute
    })
  }

  previous() {
    this.router.navigate(['..', this.page.number - 1], {
      relativeTo: this.activedRoute
    })
  }

  checkAll(letter: string) {
    this.pageForm.controls.forEach(form => {
      form.get('category').setValue(letter);
    });
  }

  private areAllP(): boolean {
    return this.pageForm.controls.every(
      (categoryForm: FormControl) => {
        return categoryForm.value.category === "P";
      }
    );
  }

  submit() {
    if (this.pageForm.valid && this.lot && this.page) {
      if (this.areAllP()) {
        this.confirmationService.confirm({
          message: 'Attention, tous les dossiers de cette page sont classés P(dossier pension), confirmer vous la validation ?',
          accept: () => {
            this.sendQuery();
          }
        });
      }else{
        this.sendQuery();
      }
    }
  }

  private sendQuery() {
    this.pageService.submit(this.lot, this.page, this.pageForm.value).subscribe(
      page => {
        this.page = page;
        this.lot.lastValidatedPage = page;
        this.messageService.add({severity:'info', summary:'La page '+this.page.number+" a été validée", detail:''})
      }
    );
  }

  navigate(event: number) {
    this.router.navigate(['..', event], {
      relativeTo: this.activedRoute
    });
  }
}
