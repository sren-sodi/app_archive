import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Page } from './page';
import { Lot } from 'src/app/app/content/lot/shared/lot';
import { Injectable } from '@angular/core';
import { PageSubmission } from '../../shared/page-submission';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
  })
export class PageService {

    private getPageUrl = environment.backURL+'/api/lots/{lot}/pages/{page}';

    public constructor(
        private http: HttpClient
    ) {
        
    }

    public getPage(lot: Lot,number: number): Observable<Page>{
        return this.http.get<Page>(this.getPageUrl.replace('{lot}',lot.number.toString()).replace('{page}',number.toString()));
    }

    public submit(lot: Lot, page: Page, data: PageSubmission): Observable<Page>{
        return this.http.patch<Page>(this.getPageUrl.replace('{lot}',lot.number.toString()).replace('{page}',page.number.toString()),{
            files : data
        });
    }
}
