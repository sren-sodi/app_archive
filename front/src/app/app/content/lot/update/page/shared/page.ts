import { File } from '../../../shared/file';

export class Page {
    number: number;
    validatedAt: Date;
    files?: Array<File>;
}
