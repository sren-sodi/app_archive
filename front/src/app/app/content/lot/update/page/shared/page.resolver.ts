import { Page } from './page';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PageService } from './page-service';
import { Injectable } from '@angular/core';

@Injectable()
export class PageResolver implements Resolve<Observable<Page>>{

    public constructor(
        private pageService: PageService
    ) {
        
    }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<Page> {
        let lot = route.parent.data.lot;
        return this.pageService.getPage(lot,parseInt(route.paramMap.get('page')));
    }
}
