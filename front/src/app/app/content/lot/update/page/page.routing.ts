import { Route } from '@angular/router';
import { PageComponent } from './page.component';
import { PageResolver } from './shared/page.resolver';

export const updatePageRouting: Route =
{
    path: ':page', component: PageComponent,
    resolve: {
        page: PageResolver
    },
};
