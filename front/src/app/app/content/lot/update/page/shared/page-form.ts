import { FormArray, FormGroup } from '@angular/forms';
import { Page } from './page';
import { FileForm } from '../../../shared/file-form';

export class PageForm extends FormArray {
    public constructor(page: Page) {
        super(page.files.map(file => new FileForm(file))
        );
    }
}
