import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Lot } from 'src/app/app/content/lot/shared/lot';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  public lot: Lot;

  constructor(
    private activedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activedRoute.data.subscribe(data => this.lot = data.lot);
  }

}
