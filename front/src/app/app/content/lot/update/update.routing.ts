import { Route } from '@angular/router';
import { UpdateComponent } from './update.component';
import { LotResolver } from '../shared/lot.resolver';
import { updatePageRouting } from './page/page.routing';

export const updateLotRouting: Route =
{
    path: ':number/update', component: UpdateComponent,
    resolve: {
        lot: LotResolver
    },
    children: [
        updatePageRouting
    ]

};