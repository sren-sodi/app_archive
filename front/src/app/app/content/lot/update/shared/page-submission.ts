import { FileSubmission } from './file-submission';

export class PageSubmission {
    files: Array<FileSubmission>;
}
