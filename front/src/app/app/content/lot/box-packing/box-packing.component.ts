import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Lot } from '../shared/lot';

@Component({
  selector: 'app-box-packing',
  templateUrl: './box-packing.component.html',
  styleUrls: ['./box-packing.component.scss']
})
export class BoxPackingComponent implements OnInit {

  public lot: Lot;

  constructor(
    private activedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activedRoute.data.subscribe(data => this.lot = data.lot);
  }

}
