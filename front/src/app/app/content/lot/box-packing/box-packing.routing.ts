import { Route } from '@angular/router';
import { BoxPackingComponent } from './box-packing.component';
import { LotResolver } from '../shared/lot.resolver';
import { createBoxPackingRouting } from './details/create.routing';
import { detailsBoxPackingRouting } from "./details/details.routing";
import { editBoxPackingRouting } from './details/edit.routing';
export const boxPackingRouting: Route =
{
    path: ':number/boxes', component: BoxPackingComponent,
    resolve: {
        lot: LotResolver
    },
    children: [
        createBoxPackingRouting,
        detailsBoxPackingRouting,
        editBoxPackingRouting,
        {
            path: '',
            redirectTo: 'create',
            pathMatch: 'full'
        },
    ],
};