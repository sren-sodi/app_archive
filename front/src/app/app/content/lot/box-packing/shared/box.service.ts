import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Lot } from '../../shared/lot';
import { Observable } from 'rxjs';
import { Box } from './box';
import { environment } from 'src/environments/environment';
import { File } from '../../shared/file';

@Injectable({
  providedIn: 'root'
})
export class BoxService {

  private getBoxesUrl = environment.backURL+'/api/lots/{lot}/boxes';
  private getBoxUrl = this.getBoxesUrl+'/{box}';
  private createBoxUrl = this.getBoxesUrl+'/new';
  private estimateNewBoxUrl = this.getBoxesUrl+'/estimate';
  private estimateBoxUrl = this.getBoxUrl+'/estimate';

  constructor(
    private http: HttpClient
  ) { }

  public createBox(lot: Lot):Observable<Box>{
    return this.http.get<Box>(this.createBoxUrl.replace('{lot}',lot.number.toString()));
  }

  public estimateNew(lot: Lot, to: File): Observable<Box>{
    return this.http.post<Box>(this.estimateNewBoxUrl.replace('{lot}',lot.number.toString()), {
      lastFile: to.number
    });
  }

  public estimateBox(lot: Lot,box: Box,  to: File): Observable<Box>{
    return this.http.post<Box>(this.estimateBoxUrl.replace('{lot}',lot.number.toString()).replace('{box}',box.number.toString()), {
      lastFile: to.number
    });
  }

  public submit(lot: Lot, to: File): Observable<Box>{
    return this.http.post<Box>(this.getBoxesUrl.replace('{lot}',lot.number.toString()), {
      lastFile: to.number
    });
  }

  public update(lot: Lot,box: Box): Observable<Box>{
    console.log(box);
    return this.http.put<Box>(this.getBoxUrl.replace('{lot}',lot.number.toString()).replace('{box}',box.number.toString()), {
      lastFile: box.lastFile.number
    });
  }

  public get(lot: Lot, number: number): Observable<Box>{
    return this.http.get<Box>(this.getBoxUrl.replace('{lot}',lot.number.toString()).replace('{box}',number.toString()));
  }
}
