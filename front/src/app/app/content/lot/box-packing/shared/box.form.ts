import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Box } from './box';

export class BoxForm extends FormGroup{
    public constructor(box: Box) {
        super({
            number: new FormControl(box.number,[
                Validators.required
            ]),
            firstFile: new FormControl(box.firstFile,[
                Validators.required
            ]),
            lastFile: new FormControl(box.lastFile,[
                Validators.required
            ])
        });
    }
} 