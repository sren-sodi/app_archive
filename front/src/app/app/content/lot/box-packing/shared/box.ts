import { File } from '../../shared/file';

export class Box {
    number: number;
    firstFile: File;
    lastFile?: File;
    nbFiles: number;
}
