import { Component, OnInit } from '@angular/core';
import { Box } from '../shared/box';
import { ActivatedRoute, Router } from '@angular/router';
import { BoxForm } from '../shared/box.form';
import { AbstractControl } from '@angular/forms';
import { Lot } from '../../shared/lot';
import { File } from '../../shared/file';
import { BoxService } from '../shared/box.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  public box: Box;
  public boxForm: BoxForm;

  public mod: string;

  public lot: Lot;

  constructor(
    private activedRoute: ActivatedRoute,
    private router: Router,
    private boxService: BoxService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.activedRoute.parent.data.subscribe(data => {
      this.lot = data.lot;
    });
    this.activedRoute.data.subscribe(data => {
      this.mod = data.mod;
      this.box = data.box;
      this.boxForm = new BoxForm(this.box);
    });
    this.lastFile.valueChanges.subscribe(value => this.estimateBox(value));
  }

  public create() {
    if (this.boxForm.valid) {
      this.boxService.submit(this.lot, this.lastFile.value).subscribe(value => {
        this.messageService.add({severity:'info', summary:'Le nouveau carton a été créé.', detail:''})
        this.lot.nbBoxes++;
        this.navigate(this.box.number);
      });
    }
  }

  public update(){
    if (this.boxForm.valid) {
      this.boxService.update(this.lot,this.box).subscribe(value => {
        this.messageService.add({severity:'info', summary:'Le carton '+this.box.number+" a été mis à jour.", detail:''})
        this.lot.nbBoxes = value.number;
        this.navigate(this.box.number);
      });
    }
  }
  /**
   * select a File
   */
  public select(file: File) {
    this.lastFile.setValue(file);
  }

  private estimateBox(to: File) {
    if(this.mod === 'create'){
      this.boxService.estimateNew(this.lot, to).subscribe(box => this.box = box);
    }else if(this.mod === 'edit'){
      this.boxService.estimateBox(this.lot,this.box, to).subscribe(box => this.box = box);
    }
  }

  get lastFile(): AbstractControl {
    return this.boxForm.get('lastFile');
  }

  previous() {
    let url = this.mod === 'edit'?['../..',this.box.number - 1, 'edit'] : ['..',this.box.number - 1];
    this.router.navigate(url, {
      relativeTo: this.activedRoute
    });
  }

  next() {
    let url = this.mod === 'edit'?['../..',this.box.number + 1, 'edit'] : ['..',this.box.number + 1];
    this.router.navigate(url, {
      relativeTo: this.activedRoute
    });
  }

  navigate(number: number) {
    let url = this.mod === 'edit'?['../..',number] : ['..',number];
    this.router.navigate(url, {
      relativeTo: this.activedRoute,
    });
  }
}
