import { Route } from '@angular/router';
import { DetailsComponent } from './details.component';
import { CreateBoxResolver } from './shared/create-box.resolver';


export const createBoxPackingRouting: Route =
{
    path: 'create',
    component: DetailsComponent,
    data:{
        mod : 'create'
    },
    resolve:{
        box: CreateBoxResolver
    }
};