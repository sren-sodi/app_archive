import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { AbstractControl, FormGroup, FormControl } from '@angular/forms';
import { Box } from '../../shared/box';
import { FileService } from '../../../shared/file.service';
import * as moment from 'moment';
import { File } from '../../../shared/file';
import { Lot } from '../../../shared/lot';

@Component({
  selector: 'app-auto-complete-agent',
  templateUrl: './auto-complete-agent.component.html',
  styleUrls: ['./auto-complete-agent.component.scss']
})
export class AutoCompleteAgentComponent implements OnInit, OnChanges {

  @Input()
  public box: Box;

  @Input()
  private lot: Lot;

  @Input()
  private mod: string;

  @Output()
  public onSelect = new EventEmitter<File>();

  public autoCompleteControl: FormGroup;
  public filesToPack: Array<File>;
  public firstnames: Array<String>;
  public commonNames: Array<String>;
  public dobs: Array<String>;

  constructor(private fileService: FileService) { }

  ngOnInit() {
    this.filesToPack = new Array<File>();
    this.autoCompleteControl = new FormGroup({
      firstname: new FormControl(this.box.lastFile ? this.box.lastFile.firstname:""),
      commonName: new FormControl(this.box.lastFile ? this.box.lastFile.commonName:""),
      dob: new FormControl(this.box.lastFile ? moment(this.box.lastFile.dob).format('DD/MM/YYYY'):""),
    });
  }

  ngOnChanges(changes: any) {
    console.log("changes ",changes);
    if(this.autoCompleteControl  && changes.box && changes.box.currentValue.lastFile){
      this.autoCompleteControl.setValue({
        firstname:  changes.box.currentValue.lastFile.firstname,
        commonName: changes.box.currentValue.lastFile.commonName,
        dob: moment(changes.box.currentValue.lastFile.dob).format('DD/MM/YYYY')
      })
    }
  }

  public select() {
    console.log("select ",this.filesToPack);
    if (this.filesToPack.length === 1) {
      this.autoCompleteControl.setValue({
        firstname:  this.firstnames[0],
        commonName: this.commonNames[0],
        dob: this.dobs[0]
      })
      console.log("emit ",this.filesToPack[0]);
      this.onSelect.emit(this.filesToPack[0]);
    }
  }

  public search() {
    console.log("search");
    return this.fileService.getFilesToPack(this.lot, this.firstname.value, this.commonName.value, this.dob.value, this.mod === 'edit'?this.box:null).subscribe(
      data => {
        this.filesToPack = data;
        this.firstnames = this.getFirstnamesOFfilesToPack();
        this.commonNames = this.getCommonNamesOFfilesToPack();
        this.dobs = this.getDobsOFfilesToPack();
      }
    );
  }

  private getFirstnamesOFfilesToPack() {
    return this.filesToPack.map(file => file.firstname).sort();
  }

  private getCommonNamesOFfilesToPack() {
    return this.filesToPack.map(file => file.commonName).sort();
  }

  private getDobsOFfilesToPack() {
    return this.filesToPack.map(file => moment(file.dob).format('DD/MM/YYYY')).sort();;
  }
  
  get firstname(): AbstractControl {
    return this.autoCompleteControl.get('firstname');
  }

  get dob(): AbstractControl {
    return this.autoCompleteControl.get('dob');
  }

  get commonName(): AbstractControl {
    return this.autoCompleteControl.get('commonName');
  }

}
