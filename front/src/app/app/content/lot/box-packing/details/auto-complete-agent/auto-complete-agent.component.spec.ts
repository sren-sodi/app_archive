import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoCompleteAgentComponent } from './auto-complete-agent.component';

describe('AutoCompleteAgentComponent', () => {
  let component: AutoCompleteAgentComponent;
  let fixture: ComponentFixture<AutoCompleteAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoCompleteAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoCompleteAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
