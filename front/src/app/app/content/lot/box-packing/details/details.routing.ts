import { Route } from '@angular/router';
import { DetailsComponent } from './details.component';
import { DetailsBoxResolver } from './shared/details-box.resolver';


export const detailsBoxPackingRouting: Route =
{
    path: ':box',
    component: DetailsComponent,
    data:{
        mod : 'details'
    },
    resolve:{
        box: DetailsBoxResolver
    }
};