import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoAgentComponent } from './info-agent.component';

describe('InfoAgentComponent', () => {
  let component: InfoAgentComponent;
  let fixture: ComponentFixture<InfoAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
