import { Component, OnInit, Input } from '@angular/core';
import { BoxForm } from '../../shared/box.form';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-info-agent',
  templateUrl: './info-agent.component.html',
  styleUrls: ['./info-agent.component.scss']
})
export class InfoAgentComponent implements OnInit {

  @Input() 
  public boxForm: BoxForm;

  @Input() 
  public label: string;

  @Input() 
  private targetControlName: string;

  constructor() { }

  ngOnInit() {

  }


  get target(): AbstractControl {
    return this.boxForm.get(this.targetControlName);
  }


}
