import { Route } from '@angular/router';
import { DetailsComponent } from './details.component';
import { DetailsBoxResolver } from './shared/details-box.resolver';


export const editBoxPackingRouting: Route =
{
    path: ':box/edit',
    component: DetailsComponent,
    data:{
        mod : 'edit'
    },
    resolve:{
        box: DetailsBoxResolver
    }
};