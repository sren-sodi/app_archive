import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Box } from '../../shared/box';
import { BoxService } from '../../shared/box.service';
import { Injectable } from '@angular/core';

@Injectable()
export class DetailsBoxResolver implements Resolve<Observable<Box>>{

    public constructor(
        private boxService: BoxService
    ) {
        
    }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<Box> {
        let lot = route.parent.data.lot;
        return this.boxService.get(lot,parseInt(route.paramMap.get('box')));
    }
}