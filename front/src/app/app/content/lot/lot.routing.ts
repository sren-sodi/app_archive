import { Route } from '@angular/router';
import { LotComponent } from './lot.component';
import { lotListRouting } from './list/lot-list.routing';
import { createLotRouting } from './create/create.routing';
import { detailsLotRouting } from './details/details.routing';
import { updateLotRouting } from './update/update.routing';
import { boxPackingRouting } from './box-packing/box-packing.routing';

export const lotRouting: Route =
{
    path: 'lots', component: LotComponent,
    children: [
        lotListRouting,
        updateLotRouting,
        createLotRouting,
        detailsLotRouting,
        boxPackingRouting,
        {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
        },
    ],
};