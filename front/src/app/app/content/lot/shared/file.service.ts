import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Lot } from './lot';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { environment } from 'src/environments/environment';
import { isString } from 'util';
import { File } from './file';
import { Box } from '../box-packing/shared/box';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private getFilesToPackUrl = environment.backURL + "/api/lots/{lot}/files"
  private getLotCountUrl = this.getFilesToPackUrl + '/count';
  private downloadShelfUrl = this.getFilesToPackUrl + '/shelves';


  constructor(
    private http: HttpClient
  ) { }

  public getFilesToPack(lot: Lot, firstname: string, commonName: string, dob: string, box?: Box): Observable<Array<File>> {
    let params = {
      'firstname': firstname,
      'commonName': commonName,
      'dob': dob
    };
    if (box) {
      params['box'] = box.number;
    }
    return this.http.get<Array<File>>(this.getFilesToPackUrl.replace('{lot}', lot.number.toString()), {
      params: params
    });
  }

  public countFiles(lot: Lot, classified?: boolean, category?: string): Observable<number> {
    return this.http.get<number>(this.getLotCountUrl.replace("{lot}", lot.number.toString()), {
      params: {
        classified: classified ? "true" : "false",
        category: isString(category) ? category : ''
      }
    })
  }

  downloadShelf(lot: Lot): Observable<any> {
    return this.http.get(this.downloadShelfUrl.replace('{lot}', lot.number.toString()), { observe: 'response', responseType: 'blob' }).pipe(
      map((res) => {
        let filename = res.headers.get('Content-Disposition').match(/filename\=([\w\-\.]+)/)[1];
        let data = {
          content: new Blob([res.body], { type: res.headers.get('Content-Type') }),
          filename: filename
        }
        return data;
      }));
  }
}
