import { Page } from '../update/page/shared/page';

export class Lot {
    number: number;
    label: string;
    state: string;
    nbFiles: number;
    nbPages: number;
    nbValidatedPages: number;
    firstNotValidatedPage: Page;
    lastValidatedPage: Page;
    nbFilesInBoxes: number;
    nbBoxes: number;
    submitedAt: Date;
    sentAt: Date;
    lastShelfExportedAt: Date;
}
