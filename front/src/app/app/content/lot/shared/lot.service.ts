import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lot } from 'src/app/app/content/lot/shared/lot';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { LotList } from 'src/app/app/content/lot/shared/lot-list';
import { map } from 'rxjs/operators';
import { LotSubmission } from 'src/app/app/content/lot/shared/lot-submission';
import * as moment from 'moment';
import { isString } from 'util';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LotService {

  private getLotUrl = environment.backURL + '/api/lots';
  private getTransitionsLotUrl = this.getLotUrl + '/{lot}/transition';
  private estimateLotUrl = this.getLotUrl+'/estimate';
  private getCreateLotUrl = this.getLotUrl + '/new';

  constructor(
    private http: HttpClient
  ) { }

  public getLot(number: number): Observable<Lot> {
    return this.http.get<Lot>(this.getLotUrl + '/' + number);
  }

  public getNewLot(): Observable<LotSubmission> {
    return this.http.get<LotSubmission>(this.getCreateLotUrl);
  }

  public getAvailableTransitions(lot: Lot): Observable<Array<String>> {
    return this.http.get<Array<String>>(this.getTransitionsLotUrl.replace('{lot}', lot.number.toString()));
  }



  /**
   * Get a partial list of Lot from :offset and a length :limit
   * @param offset 
   * @param limit 
   */
  public getLots(offset: number, limit: number): Observable<LotList> {
    return this.http.get(this.getLotUrl, {
      observe: 'response',
      params: {
        offset: offset.toString(),
        limit: limit.toString()
      }
    }).pipe(
      map((response: HttpResponse<Lot[]>) => {
        let list = new LotList();
        list.lots = response.body;
        list.totalCount = parseInt(response.headers.get('x-total-count'));
        return list;
      })
    );
  }

  public estimateLot(lot: LotSubmission): Observable<number> {

    return this.http.post<Lot>(this.estimateLotUrl, {
      withNir: lot.withNir,
      withAffiliation: lot.withAffiliation,
      dobFrom: moment(lot.dobFrom).format('YYYY-MM-DD'),
      dobTo: moment(lot.dobTo).format('YYYY-MM-DD'),
      number: lot.number
    }).pipe(
      map<any, number>(response => response.nbFiles)
    );
  }

  public submit(lot: LotSubmission): Observable<Lot> {
    return this.http.post<Lot>(this.getLotUrl, {
      withNir: lot.withNir,
      withAffiliation: lot.withAffiliation,
      dobFrom: moment(lot.dobFrom).format('YYYY-MM-DD'),
      dobTo: moment(lot.dobTo).format('YYYY-MM-DD'),
      number: lot.number
    })
  }
}
