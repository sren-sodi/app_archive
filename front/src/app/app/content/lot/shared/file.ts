export class File {
    category?: string;
    number:number;
    firstname: string;
    lastname:string;
    commonName: string;
    shelf: number;
    dob: Date;
}
