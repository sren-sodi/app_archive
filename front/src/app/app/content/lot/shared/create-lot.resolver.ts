import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Lot } from 'src/app/app/content/lot/shared/lot';
import { Observable } from 'rxjs';
import { LotService } from './lot.service';
import { Injectable } from '@angular/core';
import { LotSubmission } from './lot-submission';

@Injectable()
export class CreateLotResolver implements Resolve<Observable<LotSubmission>>{

    public constructor(
        private lotService: LotService
    ) {

    }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot 
    ): Observable<LotSubmission> {
        return this.lotService.getNewLot();
    }
}