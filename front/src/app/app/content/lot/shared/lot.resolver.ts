import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Lot } from 'src/app/app/content/lot/shared/lot';
import { Observable } from 'rxjs';
import { LotService } from './lot.service';
import { Injectable } from '@angular/core';

@Injectable()
export class LotResolver implements Resolve<Observable<Lot>>{

    public constructor(
        private lotService: LotService
    ) {

    }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<Lot> {
        return this.lotService.getLot(parseInt(route.paramMap.get('number')));
    }
}