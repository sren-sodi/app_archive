import { FormControl, FormGroup, Validators } from '@angular/forms';

export class LotForm extends FormGroup {
    constructor() {
        super(
            {
                withNir: new FormControl(false),
                withAffiliation: new FormControl(false),
                dobFrom: new FormControl(null, [
                    Validators.required
                ]),
                dobTo: new FormControl(null, [
                    Validators.required
                ]),
                number: new FormControl(null, [
                    Validators.min(0),
                    Validators.required
                ])
            }
        );
    }
}
