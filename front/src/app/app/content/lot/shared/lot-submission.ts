export class LotSubmission {
    number: number;
    withNir: boolean;
    withAffiliation: boolean;
    dobFrom: Date;
    dobTo: Date;
}
