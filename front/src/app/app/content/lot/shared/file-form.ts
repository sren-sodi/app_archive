import { FormGroup, FormControl, Validators } from '@angular/forms';
import { File } from './file';

export class FileForm extends FormGroup{
    public constructor(file :File) {
        super({
            'category': new FormControl(file.category,[
                Validators.required
            ])
        });
    }
}
