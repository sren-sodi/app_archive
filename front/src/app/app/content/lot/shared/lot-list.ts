import { Lot } from './lot';

export class LotList {
    lots: Lot[];
    totalCount: number;
}
