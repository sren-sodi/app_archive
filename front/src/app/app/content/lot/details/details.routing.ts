import { Route } from '@angular/router';
import { DetailsComponent } from './details.component';
import { LotResolver } from '../shared/lot.resolver';
import { updateLotRouting } from '../update/update.routing';

export const detailsLotRouting: Route =
{
    path: ':number', component: DetailsComponent,
    resolve:{
        lot: LotResolver
    }
};