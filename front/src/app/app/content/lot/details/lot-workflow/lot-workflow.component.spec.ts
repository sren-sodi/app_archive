import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotWorkflowComponent } from './lot-workflow.component';

describe('LotWorkflowComponent', () => {
  let component: LotWorkflowComponent;
  let fixture: ComponentFixture<LotWorkflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotWorkflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
