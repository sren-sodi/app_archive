import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';
import { LotService } from '../../shared/lot.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Lot } from 'src/app/app/content/lot/shared/lot';
import { Observable } from 'rxjs';
import { tap, flatMap } from "rxjs/operators";
import { FileService } from '../../shared/file.service';
import * as moment from 'moment';
import { saveAs } from 'file-saver';
import { FileSaverService } from 'ngx-filesaver';

@Component({
  selector: 'app-lot-workflow',
  templateUrl: './lot-workflow.component.html',
  styleUrls: ['./lot-workflow.component.scss']
})
export class LotWorkflowComponent implements OnInit {

  public lot: Lot;

  public nbClassifiedFiles: number = 0;
  public availableTransitions: Array<String> = new Array<String>(); 
  public nbPensionFiles: number;

  constructor(
    private activedRoute: ActivatedRoute,
    private lotService: LotService,
    private router: Router,
    private fileService: FileService,
    private FileSaverService: FileSaverService
      ) { }

  items: MenuItem[];

  getActiveIndex(lot: Lot): number {
    switch (lot.state) {
      case "classification": return 0;
      case "box_packing": return 1;
      case "summary": return 2;
      case "destroy": return 3;
    }
  }

  isState(state: string){
    return this.lot && this.lot.state === state;
  }

  goToPages(){
    let page = this.lot.firstNotValidatedPage ? this.lot.firstNotValidatedPage : this.lot.lastValidatedPage;

    this.router.navigate(['update',page.number.toString()],{
      relativeTo: this.activedRoute
    });
  }

  goToBoxes(){
    this.router.navigate(['boxes',(this.lot.nbBoxes).toString()],{
      relativeTo: this.activedRoute
    });
  }

  goToEditBoxes(){
    this.router.navigate(['boxes',(this.lot.nbBoxes).toString(), 'edit'],{
      relativeTo: this.activedRoute
    });
  }

  private getNbClassifiedFile(lot: Lot): Observable<number> {
    return this.fileService.countFiles(lot, true);
  }

  private getAvailableTransitions(lot: Lot): Observable<Array<String>>{
    return this.lotService.getAvailableTransitions(lot);
  }

  can(transition: string):boolean{
    return this.availableTransitions.includes(transition)
  }

  ngOnInit() {
    this.activedRoute.data.pipe(
      tap(data => this.getNbClassifiedFile(data.lot).subscribe(nb => this.nbClassifiedFiles = nb)),
      tap(data => this.getAvailableTransitions(data.lot).subscribe(availableTransitions => this.availableTransitions = availableTransitions)),
      tap(data => this.fileService.countFiles(data.lot, true, 'P').subscribe(nbPension => this.nbPensionFiles = nbPension))
    ).subscribe(data => {
      this.lot = data.lot
    })
    this.items = [
      { label: this.lot.submitedAt ? 'Terminé le '+moment(this.lot.submitedAt).format('DD/MM/YYYY'):'' },
      { label: this.lot.sentAt ? 'Terminé le '+moment(this.lot.sentAt).format('DD/MM/YYYY'):'' },
      { label: '' },
      { label: '' }
    ];
  }

  public printShelves(){
    this.fileService.downloadShelf(this.lot).pipe(
      tap(response => this.FileSaverService.save(response.content, response.filename)),
      flatMap(data => this.lotService.getLot(this.lot.number)),
    ).subscribe(lot => this.lot = lot);
  }

}
