import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotLabelComponent } from './lot-label.component';

describe('LotLabelComponent', () => {
  let component: LotLabelComponent;
  let fixture: ComponentFixture<LotLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
