import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Lot } from 'src/app/app/content/lot/shared/lot';

@Component({
  selector: 'app-lot-label',
  templateUrl: './lot-label.component.html',
  styleUrls: ['./lot-label.component.scss']
})
export class LotLabelComponent implements OnInit {

  public lot: Lot;

  constructor(
    private activedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activedRoute.data.subscribe(data => this.lot = data.lot);
  }

}
