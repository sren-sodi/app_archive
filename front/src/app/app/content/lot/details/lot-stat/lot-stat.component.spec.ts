import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotStatComponent } from './lot-stat.component';

describe('LotStatComponent', () => {
  let component: LotStatComponent;
  let fixture: ComponentFixture<LotStatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotStatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
