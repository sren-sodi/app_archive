import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Lot } from 'src/app/app/content/lot/shared/lot';
import { Observable, forkJoin, of } from 'rxjs';
import { LotService } from '../../shared/lot.service';
import { FileService } from '../../shared/file.service';

@Component({
  selector: 'app-lot-stat',
  templateUrl: './lot-stat.component.html',
  styleUrls: ['./lot-stat.component.scss']
})
export class LotStatComponent implements OnInit {

  public data: any;
  private categories: Array<string> = ['C', 'DN', 'NT', 'P', 'A'];
  public lot: Lot;

  constructor(
    private activedRoute: ActivatedRoute,
    private lotService: LotService,
    private fileService: FileService
  ) { }

  private getData(values: Array<number>): any {
    return {
      labels: this.categories,
      datasets: [
        {
          data: values,
          backgroundColor: [
            "#FDABD1",
            "#94B7E0",
            "#9BD6A6",
            '#9B89B2',
            '#E0D1A9'
          ],
          hoverBackgroundColor: [
            "#FDABD1",
            "#94B7E0",
            "#9BD6A6",
            '#9B89B2',
            '#E0D1A9'
          ]
        }]
    };
  }

  ngOnInit() {
    this.data = this.getData([0, 0, 0, 0, 0])
    this.activedRoute.data.subscribe(data => {
      this.lot = data.lot;
      let queries = this.categories.map(category =>  this.fileService.countFiles(this.lot, true, category));
      forkJoin(queries).subscribe(values => {
        this.data = this.getData(values);
      })
    });


  }

}
