import { Route } from '@angular/router';
import { ListComponent } from './list.component';

export const lotListRouting: Route =
{
    path: 'list', component: ListComponent
};