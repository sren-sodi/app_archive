import { Component, OnInit } from '@angular/core';
import { Lot } from 'src/app/app/content/lot/shared/lot';
import { LotService } from '../shared/lot.service';
import { LazyLoadEvent } from 'primeng/api/public_api';
import { Router, ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-lot-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  lots: Lot[] = new Array<Lot>();

  loading: boolean;

  totalRecords: number;

  lotNumber: string;

  constructor(
    private lotService: LotService,
    private router: Router,
    private activedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loading = false;
  }

  detailsLot(){
    this.router.navigate(['..',this.lotNumber],{
      relativeTo: this.activedRoute
    });
  }

  selectLot($event: {data: Lot}) {
      this.router.navigate(['..',$event.data.number],{
        relativeTo: this.activedRoute
      })
  }

  loadCarsLazy(event: LazyLoadEvent) {
    //this.loading = true;
    this.lotService.getLots(event.first, event.rows).subscribe(listLot => {
      this.lots = listLot.lots;
      this.totalRecords = listLot.totalCount;
    //  this.loading = false;
    }
    )

  }

}
