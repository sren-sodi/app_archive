import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'state'
})
export class StatePipe implements PipeTransform {

  private strings: Map<string,string> = new Map<string, string>([
     ["classification","Reclassement"],
     ["box_packing","Mise en carton"],
     ["summary","Synthèse"],
     ["destruction","Destruction"]
  ]);

  transform(value: string, ...args: any[]): any {
    return this.strings.has(value) ? this.strings.get(value): null;
  }

}
