import { Route } from '@angular/router';
import { ContentComponent } from './content.component';
import { lotRouting } from "./lot/lot.routing";

export const contentRouting: Route =
{
    path: '', component: ContentComponent,
    children: [
        lotRouting,
        {
            path: '',
            redirectTo: 'lots',
            pathMatch: 'full'
        },
    ]
};