import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
    providedIn: 'root',
})
export class HttpErrorInterceptor implements HttpInterceptor {

    public constructor(
        private messageService: MessageService
    ) {

    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    let errorMessage = '';
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        this.messageService.add({ severity: 'error', summary: '', detail: error.error.message });
                    }else if(error.error.errors) {
                        console.log(error.error.errors);
                        // form error
                        this.messageService.addAll(error.error.errors.map(element => {
                            return { severity: 'error', summary: 'Erreur '+error.status, detail:element };
                        }));
                    }else if(error.error) {
                        // server-side error
                       this.messageService.add({ severity: 'error', summary: 'Erreur '+error.status, detail:error.error.message });

                    } 
                    return throwError(errorMessage);
                })
            );
    }
}