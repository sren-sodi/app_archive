import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginate',
  templateUrl: './paginate.component.html',
  styleUrls: ['./paginate.component.scss']
})
export class PaginateComponent implements OnInit {

  @Input()
  public previousDisabled: boolean;

  @Input()
  public nextDisabled: boolean;

  @Input()
  public placeholder: number;

  @Input()
  public label: string;

  @Input()
  public total: number;

  @Output()
  public onClickNext: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  public onClickPrevious: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  public onEnter: EventEmitter<number> = new EventEmitter<number>();

  public input: number;

  constructor() { }

  ngOnInit() {
  }

  next() {
    this.onClickNext.emit(this.placeholder+1);
  }

  previous() {
    this.onClickPrevious.emit(this.placeholder-1);
  }

  keyDownFunction($event) {
    if ($event.key == 'Enter') {
      this.onEnter.emit(this.input);
      this.input = null;
    }
  }

}
