import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api/message';
import { MessageService } from 'primeng/api';
import { isArray } from 'util';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Archivage-front';

  msgs: Message[];

  public constructor(
    private messageService: MessageService
  ) {

  }

  ngOnInit() {
    this.msgs = new Array<Message>();
    this.messageService.messageObserver.pipe(
      map(message => {
        if(isArray(message) === false){
          return [message];
        }else{
          return message;
        }
      })
    ).subscribe(message => {
      this.msgs = message as Message[];
    });
  }

}
