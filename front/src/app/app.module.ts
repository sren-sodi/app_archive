import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './app/header/header.component';
import { MenuComponent } from './app/header/menu/menu.component';
import { ButtonModule } from 'primeng/button';
import { UserComponent } from './app/header/user/user.component';
import { ContentComponent } from './app/content/content.component';
import { LotComponent } from './app/content/lot/lot.component';
import { ListComponent } from './app/content/lot/list/list.component';
import { CreateComponent } from './app/content/lot/create/create.component';
import { DetailsComponent } from './app/content/lot/details/details.component';
import { RouterModule } from '@angular/router';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { LotLabelComponent } from './app/content/lot/details/lot-label/lot-label.component';
import { LotStatComponent } from './app/content/lot/details/lot-stat/lot-stat.component';
import { LotWorkflowComponent } from './app/content/lot/details/lot-workflow/lot-workflow.component';
import { ChartModule } from 'primeng/chart';
import { MenuModule } from 'primeng/menu';
import { StepsModule } from 'primeng/steps';
import { LotResolver } from './app/content/lot/shared/lot.resolver';
import { UpdateComponent } from './app/content/lot/update/update.component';
import { PageComponent } from './app/content/lot/update/page/page.component';
import { PageResolver } from './app/content/lot/update/page/shared/page.resolver';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MomentModule } from 'ngx-moment';
import { HttpErrorInterceptor } from './app/shared/http-error-interceptor';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MessageService, ConfirmationService } from 'primeng/api';
import { MenubarModule } from 'primeng/menubar';
import { BoxPackingComponent } from './app/content/lot/box-packing/box-packing.component';
import { DetailsComponent as DetailsBoxComponent } from "./app/content/lot/box-packing/details/details.component";
import { CreateBoxResolver } from './app/content/lot/box-packing/details/shared/create-box.resolver';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InfoAgentComponent } from './app/content/lot/box-packing/details/info-agent/info-agent.component';
import { AutoCompleteAgentComponent } from './app/content/lot/box-packing/details/auto-complete-agent/auto-complete-agent.component';
import { DetailsBoxResolver } from './app/content/lot/box-packing/details/shared/details-box.resolver';
import { PaginateComponent } from './app/paginate/paginate.component';
import { StatePipe } from './app/content/lot/list/shared/state.pipe';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { CreateLotResolver } from './app/content/lot/shared/create-lot.resolver';
import { FileSaverModule } from 'ngx-filesaver';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    UserComponent,
    ContentComponent,
    LotComponent,
    ListComponent,
    CreateComponent,
    DetailsComponent,
    LotLabelComponent,
    LotStatComponent,
    LotWorkflowComponent,
    UpdateComponent,
    PageComponent,
    BoxPackingComponent,
    CreateComponent,
    DetailsBoxComponent,
    InfoAgentComponent,
    AutoCompleteAgentComponent,
    PaginateComponent,
    StatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    TableModule,
    InputTextModule,
    ButtonModule,
    RouterModule,
    CheckboxModule,
    CalendarModule,
    FormsModule,
    ReactiveFormsModule,
    ChartModule,
    MenuModule,
    StepsModule,
    RadioButtonModule,
    MomentModule,
    MessagesModule,
    MessageModule,
    MenubarModule,
    AutoCompleteModule,
    ConfirmDialogModule,
    FileSaverModule
  ],
  providers: [
    ConfirmationService,
    MessageService,
    LotResolver,
    PageResolver,
    CreateBoxResolver,
    DetailsBoxResolver,
    CreateLotResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
