import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { contentRouting } from './app/content/content.routing';


const routes: Routes =
  [
    contentRouting,
    {
      path: '',
      redirectTo: 'content',
      pathMatch: 'full'
    },
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
