<?php


namespace App\Service\Entity;


use App\Entity\Box;
use App\Entity\File;
use App\Entity\Lot;
use App\Exception\BoxNotFoundException;
use App\Exception\FileNotFoundException;
use App\Exception\FormErrorException;
use App\Form\BoxType;
use App\Helper\LoggerAwareTrait;
use App\Repository\BoxRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Workflow\Workflow;

/**
 * Class BoxService
 * @package App\Service\Entity
 */
class BoxService
{
    const BOX_PACKING_TRANSITION = 'submit';
    const BOX_PACKING_STATE = 'box_packing';

    use LoggerAwareTrait;
    /**
     * @var BoxRepository
     */
    private $boxRepository;

    /**
     * @var FileService
     */
    private $fileService;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * BoxService constructor.
     * @param BoxRepository $boxRepository
     * @param FileService $fileService
     * @param FormFactoryInterface $formFactory
     * @param Workflow $workflow
     */
    public function __construct(
        BoxRepository $boxRepository,
        FileService $fileService,
        FormFactoryInterface $formFactory,
        Workflow $workflow
    ) {
        $this->boxRepository = $boxRepository;
        $this->fileService = $fileService;
        $this->formFactory = $formFactory;
        $this->workflow = $workflow;
    }

    /**
     * @param Box $box
     * @param File $to
     * @return Box
     */
    public function setFiles(Box $box, File $to): Box
    {
        $this->debug(sprintf("Setting files from %s to %s to box %d",
            $box->getFirstFile()->getCommonName(),
            $to->getCommonName(), $box->getNumber()));
        $files = $this->fileService->getFileRange($box->getFirstFile(), $to);
        $box->getFiles()->clear();
        foreach ($files as $file) {
            $box->addFile($file);
        }

        return $box;
    }

    /**
     * @param Lot $lot
     * @param int $number
     * @return Box
     * @throws BoxNotFoundException
     */
    public function getBox(Lot $lot, int $number): Box
    {
        $this->debug(sprintf("Getting box %d of Lot %d",
            $number, $lot->getNumber()));
        $box = $this->boxRepository->findOneBy(
            [
                'lot' => $lot,
                'number' => $number,
            ]
        );

        if (false === $box instanceof Box) {
            throw new BoxNotFoundException(sprintf("Le carton %d n'a pas été trouvé dans le lot %d.", $number,
                $lot->getNumber()));
        }

        return $box;
    }

    /**
     * @param Lot $lot
     * @param array $data
     * @return Box
     * @throws FileNotFoundException
     * @throws FormErrorException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createNewBox(Lot $lot, array $data): Box
    {
        $this->debug(sprintf("Creating new box for Lot %d",
            $lot->getNumber()), [
            'data' => $data
        ]);
        $box = $this->estimateNew($lot, $data);
        $this->boxRepository->persist($box);

        return $box;
    }

    /**
     * @param Lot $lot
     * @param array $data
     * @return Box
     * @throws FileNotFoundException
     * @throws FormErrorException
     * @throws NonUniqueResultException
     */
    public function estimateNew(Lot $lot, array $data): Box
    {
        $this->debug(sprintf("Estimating new box for Lot %d",
            $lot->getNumber()), [
            'data' => $data
        ]);
        $box = $this->initiateFromLot($lot);

        return $this->estimateBox($box, $data);
    }

    /**
     * @param Lot $lot
     * @return Box
     * @throws NonUniqueResultException
     * @throws FileNotFoundException
     */
    public function initiateFromLot(Lot $lot): Box
    {
        $this->debug(sprintf("Initialising new box for Lot %d",
            $lot->getNumber()));
        $newBox = new Box();
        $newBox->setNumber($this->getNextNumber($lot));
        $file = $this->fileService->getNextFileToPack($lot);
        $newBox->addFile($file);
        $newBox->setLot($lot);

        return $newBox;
    }

    /**
     * @param Lot $lot
     * @return int
     * @throws NonUniqueResultException
     */
    private function getNextNumber(Lot $lot): int
    {
        $this->debug(sprintf("Getting next Box number for lot %d",
            $lot->getNumber()));
        $lastBox = $this->boxRepository->getLastBox($lot);
        if ($lastBox instanceof Box) {
            return $lastBox->getNumber() + 1;
        } else {
            return 1;
        }
    }

    /**
     * @param Box $box
     * @param array $data
     * @return Box
     * @throws FormErrorException
     */
    public function estimateBox(Box $box, array $data): Box
    {
        $this->debug(sprintf("Estimating Box %d",
            $box->getNumber()), [
            'data' => $data
        ]);
        return $this->createBoxForm($box, $data);
    }

    /**
     * @param Box $box
     * @param array $data
     * @return Box
     * @throws FormErrorException
     */
    private function createBoxForm(Box $box, array $data): Box
    {
        $this->debug(sprintf("Creating form for Box %d",
            $box->getNumber()), [
            'data' => $data
        ]);
        $form = $this->formFactory->create(BoxType::class, $box);
        $form->submit($data);

        if (false === $form->isValid()) {
            throw new FormErrorException($form->getErrors(true));
        }

        return $form->getData();
    }

    /**
     * @param Box $box
     * @return bool
     */
    public function canCreateBox(Box $box): bool
    {
        $this->debug(sprintf("Checking if Box %d can be created",
            $box->getNumber()));
        return $this->workflow->can($box->getLot(),
                self::BOX_PACKING_TRANSITION) || $box->getLot()->getState() === self::BOX_PACKING_STATE;
    }

    /**
     * @param Box $box
     * @param array $data
     * @return Box
     * @throws FormErrorException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function editAndRemoveNextBoxes(Box $box, array $data): Box
    {
        $this->debug(sprintf("Editing Box %d and removing nextes",
            $box->getNumber()), [
            'data' => $data
        ]);
        $this->removeNextBoxes($box);
        $box = $this->createBoxForm($box, $data);
        $this->boxRepository->persist($box);

        return $box;
    }

    /**
     * @param Box $box
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function removeNextBoxes(Box $box)
    {
        $this->debug(sprintf("Removing boxes after Box %d",
            $box->getNumber()));
        $nextBoxes = $this->boxRepository->getNextBoxes($box);
        foreach ($nextBoxes as $nextBox) {
            $this->fileService->unboxFilesFromBox($nextBox);
            $this->boxRepository->remove($nextBox);
        }
    }
}