<?php


namespace App\Service\Entity;


use App\Entity\Lot;
use App\Entity\Page;
use App\Exception\FormErrorException;
use App\Exception\LotNotFoundException;
use App\Form\LotType;
use App\Helper\LoggerAwareTrait;
use App\Repository\LotRepository;
use App\Service\Transverse\PensionService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Workflow\Workflow;

/**
 * Class LotService
 * @package App\Service\Entity
 */
class LotService
{
    use LoggerAwareTrait;

    /**
     * @var PensionService
     */
    private $pensionService;

    /**
     * @var LotRepository
     */
    private $lotRepository;

    /**
     * @var PageService
     */
    private $pageService;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * LotService constructor.
     * @param PensionService $pensionService
     * @param LotRepository $lotRepository
     * @param PageService $pageService
     * @param FormFactoryInterface $formFactory
     * @param Workflow $workflow
     */
    public function __construct(
        PensionService $pensionService,
        LotRepository $lotRepository,
        PageService $pageService,
        FormFactoryInterface $formFactory,
        Workflow $workflow
    ) {
        $this->pensionService = $pensionService;
        $this->lotRepository = $lotRepository;
        $this->pageService = $pageService;
        $this->formFactory = $formFactory;
        $this->workflow = $workflow;
    }

    /**
     * Generate a lot based on an array of files
     * @param Lot $lot
     * @param array $files
     * @param int $numberByPage
     * @return Lot
     */
    public static function addFiles(Lot $lot, array $files, int $numberByPage): Lot
    {
        $pages = PageService::createPages($files, $numberByPage);
        foreach ($pages as $page) {
            $lot->addPage($page);
        }

        return $lot;
    }

    /**
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countLots(): int
    {
        $this->debug(sprintf("Counting lot"));
        return $this->lotRepository->countLots();
    }

    /**
     * Returns the lot with the number $number
     * @param int $number
     * @return Lot|null
     * @throws LotNotFoundException
     */
    public function getLot(int $number): Lot
    {
        $this->debug(sprintf("Getting Lot number %s", $number));

        $lot = $this->lotRepository->findOneBy(
            [
                'number' => $number,
            ]
        );
        if (false == $lot instanceof Lot) {
            throw new LotNotFoundException(sprintf("Le lot %d n'a pas été trouvé.", $number));
        }

        return $lot;
    }

    /**
     * Returns a list of lot based on filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function getLots(?int $limit = null, ?int $offset = null): array
    {
        $this->debug(sprintf("Getting Lots according to filters"), [
            'limit' => $limit,
            'offset' => $offset
        ]);

        return $this->lotRepository->findAll($limit, $offset);
    }

    /**
     * Create a Lot form and store lot if valid
     * @param array $data
     * @return Lot
     * @throws FormErrorException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createAndSaveLot(array $data): Lot
    {
        $this->debug(sprintf("Creating new Lot in order to save"), [
            'data' => $data
        ]);
        $lot = $this->createLotForm($data, false);

        return $this->saveLot($lot);
    }

    /**
     * Create a Form to build a Lot
     * @param array $data
     * @param bool $estimateOnly
     * @return Lot
     * @throws FormErrorException
     */
    private function createLotForm(array $data, bool $estimateOnly = true): Lot
    {
        $this->debug(sprintf("Creating Lot form"), [
            'data' => $data,
            'estimateOnly' => $estimateOnly
        ]);
        $lot = new Lot();
        $form = $this->formFactory->create(LotType::class, $lot, [
            'estimate_only' => $estimateOnly
        ]);
        $form->submit($data);

        if (false === $form->isValid()) {
            throw new FormErrorException($form->getErrors(true, true));
        }

        return $form->getData();
    }

    /**
     * Create a lot with its dependencies (page, file)
     * @param Lot $lot
     * @return Lot
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveLot(Lot $lot): Lot
    {
        $this->debug(sprintf("Saving Lot %s", $lot->getNumber()));
        $this->lotRepository->persist($lot);

        return $lot;
    }

    /**
     * Create a Lot form and estimate how many files would it group
     * @param array $data
     * @return FormInterface
     * @throws FormErrorException
     */
    public function createAndEstimateLot(array $data): Lot
    {
        $this->debug(sprintf("Creating new Lot in order to estimate"), [
            'data' => $data
        ]);
        return $this->createLotForm($data);
    }

    /**
     * Check if the given Lot can go from classification to box_packing
     * @param Lot $lot
     * @return bool
     */
    public function canSubmit(Lot $lot): bool
    {
        $this->debug(sprintf("Checking if Lot %s can be submitted", $lot->getNumber()));
        return $lot->getPages()->forAll(
            function (int $index, Page $page) {
                return !is_null($page->getValidatedAt());
            }
        );
    }

    public function updateShelfImport(Lot $lot)
    {
        $this->debug(sprintf("Updating shelf export date for Lot %s", $lot->getNumber()));

        $lot->setLastShelfExportedAt(new \DateTime());
        $this->saveLot($lot);
    }

    /**
     * Get list of available Transition of the given Lot
     * @param Lot $lot
     * @return array
     */
    public function getTransitions(Lot $lot): array
    {
        $this->debug(sprintf("Getting available transitions for Lot %s", $lot->getNumber()));

        return $this->workflow->getEnabledTransitions($lot);
    }

    /**
     * @return Lot
     * @throws NonUniqueResultException
     */
    public function createNew(): Lot
    {
        $this->debug(sprintf("Creating new Lot"));

        $highestLot = $this->lotRepository->getHighestLot();
        $newLot = new Lot();

        if ($highestLot instanceof Lot) {
            $newLot->setNumber($highestLot->getNumber() + 1);
        } else {
            $newLot->setNumber(1);
        }
        return $newLot;
    }
}