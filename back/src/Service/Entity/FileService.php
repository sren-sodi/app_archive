<?php


namespace App\Service\Entity;


use App\Entity\Box;
use App\Entity\File;
use App\Entity\FileCategory;
use App\Entity\Lot;
use App\Exception\FileNotFoundException;
use App\Helper\LoggerAwareTrait;
use App\Repository\FileRepository;
use App\Service\Transverse\CsvExportService;
use CodeInc\StripAccents\StripAccents;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class FileService
 * @package App\Service\Entity
 */
class FileService
{

    use LoggerAwareTrait;

    /**
     *
     */
    const SHELF_EXPORT_HEADER = [
        'nom_patro',
        'shelf'
    ];

    /**
     * @var FileRepository
     */
    private $fileRepository;


    private $lotService;

    /**
     * FileService constructor.
     * @param FileRepository $fileRepository
     */
    public function __construct(FileRepository $fileRepository, LotService $lotService)
    {
        $this->fileRepository = $fileRepository;
        $this->lotService = $lotService;
    }

    /**
     * @param array $arr
     */
    public static function sortFiles(array &$arr)
    {
        usort($arr, function (File $a, File $b) {
            if ($a->getCommonName() !== $b->getCommonName()) {
                return self::compare($a->getCommonName(), $b->getCommonName());
            } else {
                return self::compare($a->getFirstname(), $b->getFirstname());
            }
        });
    }

    /**
     * @param string $a
     * @param string $b
     * @return int
     */
    private static function compare(string $a, string $b): int
    {
        return strcasecmp(self::clear($a), self::clear($b));
    }

    /**
     * @param string $string
     * @return string|string[]
     */
    private static function clear(string $string)
    {
        $str = StripAccents::strip($string);
        //spaces must be delete to match BDD sort function
        $replace = '';
        $str = str_replace(' ', $replace, $str);
        return $str;
    }

    /**
     * @param File $file
     * @return string|null
     */
    public static function toStringCategory(File $file): ?string
    {
        if ($file->getCategory() instanceof FileCategory) {
            return $file->getCategory()->getCode();
        } else {
            return null;
        }
    }

    /**
     * @param string $number
     * @return File
     * @throws FileNotFoundException
     */
    public
    function findByNumber(
        int $number
    ): File {
        $this->debug(sprintf("Getting File with number %d",
            $number));
        $file = $this->fileRepository->findOneBy(
            [
                'number' => $number,
            ]
        );

        if (false === $file instanceof File) {
            throw new FileNotFoundException(sprintf("Le dossier %d n'a pas été trouvé.", $number));
        }

        return $file;
    }

    /**
     * @param Lot $lot
     * @param bool|null $classified
     * @param string|null $categoryCode
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public
    function getNbFiles(
        Lot $lot,
        ?bool $classified = null,
        ?string $categoryCode = null
    ): int {
        $this->debug(sprintf("Getting number of files of lot %d",
            $lot->getNumber()), [
            'classified' => $classified,
            'categoryCode' => $categoryCode
        ]);
        return $this->fileRepository->getNbFiles($lot, $classified, $categoryCode);
    }

    /**
     * @param Lot $lot
     * @return File|null
     * @throws NonUniqueResultException
     * @throws FileNotFoundException
     */
    public
    function getNextFileToPack(
        Lot $lot
    ): File {
        $this->debug(sprintf("Getting next File to pack for lot %d",
            $lot->getNumber()));
        $file = $this->fileRepository->getNextFileToPack($lot);
        if (false === $file instanceof File) {
            throw new FileNotFoundException(sprintf("Aucun dossier a mettre en carton pour ce Lot n'a pas été trouvé."));
        }

        return $file;
    }

    /**
     * @param Lot $lot
     * @return array
     */
    public
    function getFilesToPack(
        Lot $lot
    ): array {
        $this->debug(sprintf("Getting next Files to pack for lot %d",
            $lot->getNumber()));
        return $this->fileRepository->getFilesToPack($lot);
    }

    /**
     * @param File $from
     * @param File $to
     * @return array
     */
    public
    function getFileRange(
        File $from,
        File $to
    ): array {
        $this->debug(sprintf("Getting files in range from %s to %s",
            $from, $to));
        return $this->fileRepository->getFileRange($from, $to);
    }

    /**
     * @param File $from
     * @param File $to
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public
    function estimateRange(
        File $from,
        File $to
    ): int {
        $this->debug(sprintf("Estimating files in range from %s to %s",
            $from, $to));
        return $this->fileRepository->estimateRange($from, $to);
    }

    /**
     * @param Lot $lot
     * @param string $firstname
     * @param string $commonName
     * @param string $dob
     * @param Box|null $box
     * @return array
     * @throws NonUniqueResultException
     */
    public
    function getFilteredFilesToPack(
        Lot $lot,
        string $firstname,
        string $commonName,
        string $dob,
        ?Box $box = null
    ): array {
        $this->debug(sprintf("Getting files according to filters for the lot %s",
            $lot->getNumber()), [
            'firstname' => $firstname,
            'commonName' => $commonName,
            'dob' => $dob,
            'box' => $box instanceof Box ? $box->getNumber() : ''
        ]);
        if ($box instanceof Box) {
            $file = $box->getFirstFile();
        } else {
            $file = $this->fileRepository->getNextFileToPack($lot);
        }

        return $this->fileRepository->getFilteredFilesToPack($file, $firstname, $commonName, $dob);
    }

    /**
     * @param Box $box
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public
    function unboxFilesFromBox(
        Box $box
    ) {
        $this->debug(sprintf("Unboxing files from box number %s",
            $box->getNumber()));
        $files = $box->getFiles()->toArray();
        foreach ($files as $file) {
            $box->removeFile($file);
        }
        $this->fileRepository->massPersist($files);
    }

    public function exportShelf(Lot $lot): string
    {
        $this->debug(sprintf("Exporting shelves data from lot number %s",
            $lot->getNumber()));
        $this->lotService->updateShelfImport($lot);
        $files = $this->fileRepository->getFistOfEachShelf($lot);
        return self::buildShelvesExport($files);
    }

    /**
     * @param array $files
     * @return string
     */
    public static function buildShelvesExport(array $files): string
    {
        $array = array_map(function (File $file) {
            return self::buildShelfExport($file);
        }, $files);
        array_unshift($array, self::SHELF_EXPORT_HEADER);
        return CsvExportService::pack($array);
    }

    /**
     * @param File $file
     * @return array
     */
    private
    static function buildShelfExport(
        File $file
    ): array {
        return [
            strtoupper($file->getCommonName()),
            $file->getShelf(),
        ];
    }

    /**
     * @param File $file
     */
    public
    function persist(
        File $file
    ) {
        $this->debug(sprintf("Persisting File number %s",
            $file->getNumber()));
        $this->fileRepository->persist($file);
    }

}