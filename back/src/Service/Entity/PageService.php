<?php


namespace App\Service\Entity;


use App\Entity\FileCategory;
use App\Entity\Lot;
use App\Entity\Page;
use App\Exception\FormErrorException;
use App\Exception\PageNotFoundException;
use App\Form\PageType;
use App\Helper\LoggerAwareTrait;
use App\Repository\PageRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class PageService
 * @package App\Service\Entity
 */
class PageService
{

    use LoggerAwareTrait;

    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * PageService constructor.
     * @param PageRepository $pageRepository
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(PageRepository $pageRepository, FormFactoryInterface $formFactory)
    {
        $this->pageRepository = $pageRepository;
        $this->formFactory = $formFactory;
    }

    /**
     * Create an array of pages based on a list of files
     * @param array $files
     * @param int $numberByPage
     * @return array
     */
    public static function createPages(array $files, int $numberByPage): array
    {
        $pages = [];
        $chunkedFiles = empty($files) ? [[]] : array_chunk($files, max(1, $numberByPage));
        foreach ($chunkedFiles as $key => $files) {
            $pages[] = self::createPage($key, $files);
        }

        return $pages;
    }

    /**
     * Create a single page with files attached
     * @param int $number
     * @param array $files
     * @return Page
     */
    public static function createPage(int $number, array $files): Page
    {
        $page = new Page();
        $page->setNumber($number + 1);
        foreach ($files as $file) {
            $page->addFile($file);
        }

        return $page;
    }

    /**
     * Affecte a fileCategory to all Files in the given Page
     * @param Page $page
     * @param FileCategory $category
     */
    public static function affectCategory(Page $page, ?FileCategory $category)
    {
        foreach ($page->getFiles() as $file) {
            $file->setCategory($category);
        }
    }

    /**
     * Create a form based on the given Page, modified with the submited data
     * @param Page $page
     * @param array $data
     * @return FormInterface
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws FormErrorException
     */
    public function submit(Page $page, array $data): Page
    {
        $this->debug(sprintf("Editing Page %s", $page->getNumber()), [
            'data' => $data
        ]);

        $form = $this->formFactory->create(PageType::class, $page);
        $form->submit($data);

        if ($form->isValid()) {
            $this->persist($form->getData());
        } else {
            throw new FormErrorException($form->getErrors());
        }

        return $form->getData();
    }

    /**
     * Persist a page
     * @param Page $page
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Page $page)
    {
        $this->debug(sprintf("Persisting Page %s", $page->getNumber()));
        $this->pageRepository->persist($page);
    }

    /**
     * Get a page of a lot
     * @param Lot $lot
     * @param int $number
     * @return Page
     * @throws PageNotFoundException
     */
    public function getPage(Lot $lot, int $number): Page
    {
        $this->debug(sprintf("Getting Page %s for Lot %s", $number, $lot->getNumber()));

        $page = $this->pageRepository->findOneBy(
            [
                'lot' => $lot,
                'number' => $number,
            ]
        );

        if (false === $page instanceof Page) {
            throw new PageNotFoundException(sprintf("La page %d n'a pas été trouvée dans le lot %d.", $number,
                $lot->getNumber()));
        }

        return $page;
    }
}