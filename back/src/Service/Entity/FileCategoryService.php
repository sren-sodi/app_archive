<?php


namespace App\Service\Entity;


use App\Entity\FileCategory;
use App\Exception\FileCategoryNotFound;
use App\Helper\LoggerAwareTrait;
use App\Repository\FileCategoryRepository;

/**
 * Class FileCategoryService
 * @package App\Service\Entity
 */
class FileCategoryService
{

    use LoggerAwareTrait;

    /**
     * @var FileCategoryRepository
     */
    private $fileCategoryRepository;

    /**
     * FileCategoryService constructor.
     * @param FileCategoryRepository $fileCategoryRepository
     */
    public function __construct(FileCategoryRepository $fileCategoryRepository)
    {
        $this->fileCategoryRepository = $fileCategoryRepository;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $this->debug("Getting all FileCategories");
        return $this->fileCategoryRepository->findAll();
    }

    /**
     * @param string $code
     * @return FileCategory
     * @throws FileCategoryNotFound
     */
    public function findByCode(string $code): FileCategory
    {
        $this->debug(sprintf("Getting FileCategory with code %s",
            $code));
        $category = $this->fileCategoryRepository->findOneBy(
            [
                'code' => $code,
            ]
        );

        if (false === $category instanceof FileCategory) {
            throw new FileCategoryNotFound(sprintf("La catégorie de dossier %s n'a pas été trouvée.", $code));
        }

        return $category;
    }
}