<?php


namespace App\Service\Transverse;


use App\Entity\File;
use App\Entity\Lot;
use App\Helper\LoggerAwareTrait;
use App\Service\Entity\FileService;
use DateInterval;
use DateTime;
use DateTimeInterface;
use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class PensionService
 * @package App\Service\Transverse
 */
class PensionService
{

    use LoggerAwareTrait;

    const FILE_BY_SHELF = 250;
    /**
     * @var string
     */
    private $pensionAPI;

    /**
     * PensionService constructor.
     * @param string $pensionAPI
     */
    public function __construct(string $pensionAPI)
    {
        $this->pensionAPI = $pensionAPI;
    }

    /**
     * Get all Files of the Lot
     * https://symfony.com/doc/current/components/http_client.html#concurrent-requests
     * @param Lot $lot
     * @return array
     * @throws Exception
     * Returns from Pension API, the list of File according to the lot
     */
    public function getFilesForLot(Lot $lot): array
    {
        $this->debug(sprintf("Getting Files from PENSION for Lot %s", $lot->getNumber()));

        return [];
        $filesResponses = self::sendRequests($lot);

        /**
         * For each Request, build an array of File object
         */
        $files = self::mergeResponses($filesResponses);
        FileService::sortFiles($files);
        self::affectShelfNumber($files);
    }

    /**
     * @param Lot $lot
     * @return array
     * @throws Exception
     */
    private static function sendRequests(Lot $lot): array
    {
        return array_map(
            function (array $params) {
                return $this->getFiles($params);
            },
            self::buildQueriesParam($lot)
        );
    }

    /**
     * @param Lot $lot
     * @return array
     * @throws Exception
     */
    public static function buildQueriesParam(Lot $lot): array
    {
        $queries = [];
        $from = $lot->getDobFrom();
        /**
         * laps if 15 days
         */
        /**
         * @var DateTime $to
         */
        $to = clone $from;
        $to->add(new DateInterval('P14D'));
        while ($from < $lot->getDobTo()) {
            $queries[] = self::buildQuery($lot, false, $from, $to);
            $from = clone $to->add(new DateInterval('P1D'));
            $to->add(new DateInterval('P14D'));
            $to = clone min($to, $lot->getDobTo());

        }

        return $queries;
    }

    /**
     * @param Lot $lot
     * @param bool $count
     * @param DateTimeInterface|null $from
     * @param DateTimeInterface|null $to
     * @return array
     */
    private static function buildQuery(
        Lot $lot,
        bool $count,
        ?DateTimeInterface $from = null,
        DateTimeInterface $to = null
    ): array {
        if (is_null($from)) {
            $from = $lot->getDobFrom();
        }

        if (is_null($to)) {
            $to = $lot->getDobTo();
        }

        return [
            'c1' => $from->format('d/m/Y') . '-' . $to->format('d/m/Y'),
            'c2' => ($lot->getWithNir() ? 'avec' : 'sans') . ' NIR',
            'c3' => ($lot->getWithNir() ? 'avec' : 'sans') . ' affiliation',
            'c4' => $count ? '1' : '0',
        ];
    }

    private static function mergeResponses(array $filesResponses): array
    {
        return array_merge(
            array_map(
                function (ResponseInterface $response) {
                    return array_map(
                        function (array $data) {
                            return self::buildFile($data);
                        },
                        $response->toArray()
                    );
                },
                $filesResponses
            )
        );
    }

    /**
     * Build a file based on Pension array
     * @param array $data
     * @return File
     * @throws Exception
     */
    private static function buildFile(array $data): File
    {
        $file = new File();
        $file->setNumber($data['ndossier']);
        $file->setLastname($data['nomfamille']);
        $file->setCommonName($data['nomusuel']);
        $file->setFirstname($data['prenom']);
        $file->setDob(new DateTime($data['datenaissance']));

        return $file;
    }

    private static function affectShelfNumber(array &$files)
    {
        array_walk($files, function (File &$file, $key) {
            $file->setShelf(($key + 1) % self::FILE_BY_SHELF);
        });
    }

    /**
     * Returns from Pension API,the number of Files according to the lot
     * @param Lot $lot
     * @return int
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function countFilesForLot(Lot $lot): int
    {
        $this->debug(sprintf("Getting number of Files from PENSION for Lot %s", $lot->getNumber()));

        //ajouter param c4=1 pour le count
        return 10;
        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            $this->pensionAPI,
            [
                'query' => self::buildQuery($lot, true),
            ]
        );

        return $response->getContent();
    }

    /**
     * Get a partial list of file from a date to another
     * @param Lot $lot
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    private function getFiles(array $params): ResponseInterface
    {
        $client = HttpClient::create();

        return $client->request(
            "GET",
            $this->pensionAPI,
            [
                'query' => $params,
            ]
        );
    }
}