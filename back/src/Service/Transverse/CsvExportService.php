<?php


namespace App\Service\Transverse;


/**
 * Class CsvExportService
 * @package App\Service\Transverse
 */
class CsvExportService
{

    const CSV_SEPARATOR = ",";
    const CSV_EOL = PHP_EOL;

    /**
     * Glue csv lines into a string content
     * @param array $csvLines
     * @return string
     */
    public static function pack(array $csvLines): string
    {
        return implode(self::CSV_EOL, array_map(function (array $line) {
            return implode(self::CSV_SEPARATOR, $line);
        }, $csvLines));
    }
}