<?php


namespace App\DataTransformer;


use App\Exception\FileCategoryNotFound;
use App\Service\Entity\FileCategoryService;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FileCategoryViewTransformer implements DataTransformerInterface
{
    private $fileCategoryService;

    public function __construct(FileCategoryService $fileCategoryService)
    {
        $this->fileCategoryService = $fileCategoryService;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        if (false === is_string($value)) {
            throw new TransformationFailedException(sprintf("string expected, %d given.", gettype($value)));
        }

        try {
            return $this->fileCategoryService->findByCode($value);
        } catch (FileCategoryNotFound $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($value)
    {
        return $value;
    }
}