<?php


namespace App\DataTransformer;


use App\Entity\File;
use App\Exception\FileNotFoundException;
use App\Service\Entity\FileService;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FileTransformer implements DataTransformerInterface
{

    private $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
//        dump(__CLASS__.' ['.$value.']');

        if (null === $value) {
            return null;
        }

        /**
         * @var File $value
         */
        return $value->getNumber();
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($value)
    {
        $intVal = 0;
        if (is_numeric($value)) {
            $intVal = intval($value);
        }
        if ($intVal === 0) {
            throw new TransformationFailedException(sprintf("int expected, %d given.", gettype($value)));
        }

        if (empty($value)) {
            return null;
        }

        try {
            return $this->fileService->findByNumber($intVal);
        } catch (FileNotFoundException $e) {
//            dump(sprintf(
//                'A file with number "%s" does not exist.',
//                $value
//            ), 0, $e);

            throw new TransformationFailedException(
                sprintf(
                    'A file with number "%s" does not exist.',
                    $value
                ), 0, $e
            );
        }
    }
}