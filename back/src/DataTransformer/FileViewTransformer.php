<?php


namespace App\DataTransformer;


use App\Exception\FileNotFoundException;
use App\Service\Entity\FileService;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FileViewTransformer implements DataTransformerInterface
{
    private $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        if (true === empty($value)) {
            return null;
        }

        $intVal = 0;
        if (is_numeric($value)) {
            $intVal = intval($value);
        }
        if ($intVal === 0) {
            throw new TransformationFailedException(sprintf("int expected, %d given.", gettype($value)));
        }

        try {
            return $this->fileService->findByNumber($value);
        } catch (FileNotFoundException $e) {
//            dump(__CLASS__.' '.$value.' not found');

            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($value)
    {
//        dump(__CLASS__.' ['.$value.']');

        return $value;
    }
}