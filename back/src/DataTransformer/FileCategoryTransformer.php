<?php


namespace App\DataTransformer;


use App\Entity\FileCategory;
use App\Exception\FileCategoryNotFound;
use App\Service\Entity\FileCategoryService;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FileCategoryTransformer implements DataTransformerInterface
{
    private $fileCategoryService;

    public function __construct(FileCategoryService $fileCategoryService)
    {
        $this->fileCategoryService = $fileCategoryService;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        if (null === $value) {
            return '';
        }

        /**
         * @var FileCategory $value
         */
        return $value->getCode();
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($value)
    {
        if (false === is_string($value)) {
            throw new TransformationFailedException(sprintf("string expected, %d given.", gettype($value)));
        }

        try {
            return $this->fileCategoryService->findByCode($value);
        } catch (FileCategoryNotFound $e) {
            return null;
        }
    }
}