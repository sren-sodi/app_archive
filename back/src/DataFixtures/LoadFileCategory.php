<?php


namespace App\DataFixtures;


use App\Entity\FileCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFileCategory extends Fixture
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $category = [
            'C',
            'D',
            'NT',
            'P',
            'A',
        ];

        foreach ($category as $cat) {
            $c = new FileCategory();
            $c->setCode($cat);
            $manager->persist($c);
            $this->setReference('category-' . $cat, $c);
        }
        $manager->flush();
    }
}