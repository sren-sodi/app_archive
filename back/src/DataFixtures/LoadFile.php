<?php


namespace App\DataFixtures;


use App\Entity\File;
use App\Entity\FileCategory;
use App\Entity\Lot;
use App\Entity\Page;
use App\Service\Entity\LotService;
use App\Service\Entity\PageService;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class LoadFile extends Fixture implements DependentFixtureInterface
{

    private const NB_FILE = 1000;
    private const NB_BY_PAGE = 20;
    private const NB_LOT = 5;

    /**
     * @var Generator
     */
    static $faker;

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        self::$faker = Factory::create('fr_FR');;

        for ($i = 0; $i < self::NB_LOT; $i++) {
            $lot = self::generateLot($i);
            $manager->persist($lot);
            $manager->flush();
            $manager->clear();
        }
    }

    private function generateLot(int $number): Lot
    {

        $files = [];
        for ($i = 0; $i < self::NB_FILE; $i++) {
            $files[] = $this->getRandomFile($number * self::NB_FILE + $i);
        }

        $lot = new Lot();
        $lot->setState("");
        $lot->setNumber($number + 1);
        $lot->setWithNir(self::$faker->boolean());
        $lot->setWithAffiliation(self::$faker->boolean());
        $lot->setDobTo(self::$faker->dateTimeAD());
        $lot->setDobFrom(self::$faker->dateTimeAD($lot->getDobTo()));
        $lot = LotService::addFiles($lot, $files, self::NB_BY_PAGE);

        if ($number % 2 === 0) {
            $this->validate($lot);
        }

        return $lot;
    }

    private function getRandomFile(int $indice): File
    {
        $file = new File();
        $file->setNumber($indice + 1);
        $file->setDob(self::$faker->dateTime());
        $file->setShelf(1 + intval($indice / 150));
        $file->setFirstname(self::$faker->firstName());
        $file->setLastname(self::$faker->lastName);
        $file->setCommonName(self::$faker->lastName);

        return $file;
    }

    private function validate(Lot $lot)
    {
        foreach ($lot->getPages() as $page) {
            /**
             * @var Page $page
             */
            PageService::affectCategory($page, $this->generateCategory());
            $page->setValidatedAt(new DateTime());
        }
    }

    private function generateCategory(): FileCategory
    {
        $categories = [
            'C',
            'D',
            'NT',
            'P',
            'A',
        ];

        return $this->getCategory($categories[array_rand($categories)]);
    }

    private function getCategory(string $code): ?FileCategory
    {
        return $this->getReference("category-" . $code);
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            LoadFileCategory::class,
        ];
    }
}