<?php


namespace App\NamingStrategy;


use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;

/**
 * Class MenNamingStrategy
 * @package App\NamingStrategy
 */
class MenNamingStrategy extends UnderscoreNamingStrategy
{
    /**
     *
     */
    const APP_NAME = "ARCHIVE";

    /**
     * MenNamingStrategy constructor.
     */
    public function __construct()
    {
        parent::__construct(CASE_UPPER, true);
    }

    /**
     * @inheritDoc
     * @throws \ReflectionException
     */
    function propertyToColumnName($propertyName, $className = null)
    {
        if (self::isTrigramed($className)) {
            $reflectionMethod = new \ReflectionMethod($className, "getTrigram");
            $trigram = $reflectionMethod->invoke(null);
        } else {
            throw new \Exception("Entity must have a trigram to match MEN NamyStrategy");
        }
        return strtoupper($trigram) . '_' . parent::propertyToColumnName($propertyName, $className);
    }

    /**
     * @param string $className
     * @return bool
     * @throws \ReflectionException
     */
    private function isTrigramed(string $className): bool
    {
        $class = new \ReflectionClass($className);
        return $class->implementsInterface(Trigramed::class);
    }

    /**
     * @param string $propertyName
     * @param string $embeddedColumnName
     * @param null $className
     * @param null $embeddedClassName
     * @return string
     */
    public function embeddedFieldToColumnName(
        $propertyName,
        $embeddedColumnName,
        $className = null,
        $embeddedClassName = null
    ) {
        return parent::classToTableName($className) . '_' . parent::embeddedFieldToColumnName($propertyName,
                $embeddedColumnName, $className, $embeddedClassName);
    }

    /**
     * @param string $propertyName
     * @param null $className
     * @return string
     */
    public function joinColumnName($propertyName, $className = null)
    {
        return parent::joinColumnName($propertyName, $className);
    }

    /**
     * @param string $sourceEntity
     * @param string $targetEntity
     * @param null $propertyName
     * @return string
     */
    public function joinTableName($sourceEntity, $targetEntity, $propertyName = null)
    {
        return $this->classToTableName($targetEntity) . '_' . $this->referenceColumnName();
    }

    /**
     * @inheritDoc
     */
    function classToTableName($className)
    {
        return self::APP_NAME . '_' . parent::classToTableName($className);
    }

    /**
     * @return string
     */
    public function referenceColumnName()
    {
        return parent::referenceColumnName();
    }

    /**
     * @param string $entityName
     * @param null $referencedColumnName
     * @return string
     */
    public function joinKeyColumnName($entityName, $referencedColumnName = null)
    {
        return parent::joinKeyColumnName($entityName, $referencedColumnName);
    }

}