<?php


namespace App\EventSubscriber\Transition;


use App\Entity\Lot;
use DateTime;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

/**
 * Class LogDateTimeTransition
 * @package App\EventSubscriber\Transition
 */
class LogDateTimeTransition implements EventSubscriberInterface
{

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'workflow.archivage.transition.submit' => 'onSubmit',
            'workflow.archivage.transition.send' => 'onSend',
        ];
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function onSubmit(Event $event)
    {
        /**
         * @var Lot $lot
         */
        $lot = $event->getSubject();
        $lot->setSubmitedAt(new DateTime());
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function onSend(Event $event)
    {
        /**
         * @var Lot $lot
         */
        $lot = $event->getSubject();
        $lot->setSentAt(new DateTime());
    }
}