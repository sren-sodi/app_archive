<?php


namespace App\EventSubscriber\WorkflowGuard;


use App\Entity\Lot;
use App\Service\Entity\LotService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\GuardEvent;

/**
 * Class SubmitLotGuardListener
 * @package App\EventSubscriber\WorkflowGuard
 */
class SubmitLotGuardListener implements EventSubscriberInterface
{
    /**
     * @var LotService
     */
    private $lotService;

    /**
     * SubmitLotGuardListener constructor.
     * @param LotService $lotService
     */
    public function __construct(LotService $lotService)
    {
        $this->lotService = $lotService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'workflow.archivage.guard.submit' => ['guardReview'],
        ];
    }

    /**
     * @param GuardEvent $event
     */
    public function guardReview(GuardEvent $event)
    {
        /**
         * @var Lot $lot
         */
        $lot = $event->getSubject();
        $canSubmit = $this->lotService->canSubmit($lot);

        if (false === $canSubmit) {
            $event->setBlocked(true);
        }
    }
}