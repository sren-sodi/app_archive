<?php


namespace App\Exception;


use Exception;

/**
 * Class FileCategoryNotFound
 * @package App\Exception
 */
class FileCategoryNotFound extends Exception
{

}