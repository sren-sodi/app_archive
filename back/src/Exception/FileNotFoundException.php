<?php


namespace App\Exception;


use Exception;

/**
 * Class FileNotFoundException
 * @package App\Exception
 */
class FileNotFoundException extends Exception
{

}