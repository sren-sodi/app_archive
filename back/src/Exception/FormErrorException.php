<?php


namespace App\Exception;


use Exception;
use Symfony\Component\Form\FormErrorIterator;
use Throwable;

/**
 * Class FormErrorException
 * @package App\Exception
 */
class FormErrorException extends Exception
{

    /**
     * @var FormErrorIterator
     */
    private $formErrorIterator;

    /**
     * FormErrorException constructor.
     * @param FormErrorIterator $formErrorIterator
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        FormErrorIterator $formErrorIterator,
        $message = "",
        $code = 0,
        Throwable $previous = null
    ) {
        $this->formErrorIterator = $formErrorIterator;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return FormErrorIterator
     */
    public function getFormError(): FormErrorIterator
    {
        return $this->formErrorIterator;
    }
}