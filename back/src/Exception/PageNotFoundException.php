<?php


namespace App\Exception;


use Exception;

/**
 * Class PageNotFoundException
 * @package App\Exception
 */
class PageNotFoundException extends Exception
{

}