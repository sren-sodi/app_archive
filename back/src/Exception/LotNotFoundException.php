<?php


namespace App\Exception;


use Exception;

/**
 * Class LotNotFoundException
 * @package App\Exception
 */
class LotNotFoundException extends Exception
{

}