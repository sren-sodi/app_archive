<?php


namespace App\Converter;


use App\Entity\Lot;
use App\Entity\Page;
use App\Exception\PageNotFoundException;
use App\Service\Entity\PageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageParamConverter implements ParamConverterInterface
{
    private $pageService;

    public function __construct(PageService $pageService)
    {
        $this->pageService = $pageService;
    }

    function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        if (!$request->attributes->has($param)) {
            return false;
        }

        $value = $request->attributes->get($param);

        if (!$value && $configuration->isOptional()) {
            $request->attributes->set($param, null);

            return true;
        }

        if (false === ctype_digit($value)) {
            throw new BadRequestHttpException("Le numéro de Page ne possède pas le bon format.");
        }

        try {
            $page = $this->pageService->getPage($this->findRequestLot($request), $value);
            $request->attributes->set($param, $page);

        } catch (PageNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        }

        return true;
    }

    private function findRequestLot(Request $request): Lot
    {
        return $request->attributes->get("lot");
    }

    function supports(ParamConverter $configuration)
    {
        if (null === $configuration->getClass()) {
            return false;
        }

        return $configuration->getClass() === Page::class;
    }
}