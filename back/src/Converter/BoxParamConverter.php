<?php


namespace App\Converter;


use App\Entity\Box;
use App\Entity\Lot;
use App\Exception\BoxNotFoundException;
use App\Service\Entity\BoxService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BoxParamConverter implements ParamConverterInterface
{
    private $boxService;

    public function __construct(BoxService $boxService)
    {
        $this->boxService = $boxService;
    }

    function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        if (!$request->attributes->has($param)) {
            return false;
        }

        $value = $request->attributes->get($param);

        if (!$value && $configuration->isOptional()) {
            $request->attributes->set($param, null);

            return true;
        }

        if (false === ctype_digit($value)) {
            throw new BadRequestHttpException("Le numéro de carton ne possède pas le format attendu.");
        }

        try {
            $page = $this->boxService->getBox($this->findRequestLot($request), $value);
            $request->attributes->set($param, $page);

        } catch (BoxNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        }

        return true;
    }

    private function findRequestLot(Request $request): Lot
    {
        return $request->attributes->get("lot");
    }

    function supports(ParamConverter $configuration)
    {
        if (null === $configuration->getClass()) {
            return false;
        }

        return $configuration->getClass() === Box::class;
    }
}