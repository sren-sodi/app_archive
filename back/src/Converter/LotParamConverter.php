<?php


namespace App\Converter;


use App\Entity\Lot;
use App\Exception\LotNotFoundException;
use App\Service\Entity\LotService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LotParamConverter implements ParamConverterInterface
{

    private $lotService;

    public function __construct(LotService $lotService)
    {
        $this->lotService = $lotService;
    }

    function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        if (!$request->attributes->has($param)) {
            return false;
        }

        $value = $request->attributes->get($param);

        if (!$value && $configuration->isOptional()) {
            $request->attributes->set($param, null);

            return true;
        }

        if (false === ctype_digit($value)) {
            throw new BadRequestHttpException("Le numéro de lot ne possède pas le format attendu");
        }

        try {
            $lot = $this->lotService->getLot(intval($value));
            $request->attributes->set($param, $lot);

        } catch (LotNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        }

        return true;
    }

    function supports(ParamConverter $configuration)
    {
        if (null === $configuration->getClass()) {
            return false;
        }

        return $configuration->getClass() === Lot::class;
    }
}