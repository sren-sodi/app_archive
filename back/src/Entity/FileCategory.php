<?php

namespace App\Entity;

use App\NamingStrategy\Trigramed;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileCategoryRepository")
 */
class FileCategory implements Trigramed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Identifiant technique"}
     * )
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=10,
     *     options={"comment":"Code"}
     * )
     */
    private $code;

    public static function getTrigram(): string
    {
        return "afc";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
