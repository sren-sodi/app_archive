<?php

namespace App\Entity;

use App\NamingStrategy\Trigramed;
use App\Service\Entity\FileService;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 */
class File implements Trigramed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Identifiant technique"}
     * )
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="integer",
     *     length=10,
     *     options={"comment":"Nombre"}
     * )
     * @Assert\NotBlank(
     *     message="Le numéro de fichier est obligatoire."
     * )
     * @JMS\Groups({"list","details"})
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Page", inversedBy="files")
     * @ORM\JoinColumn(name="APA_ID", referencedColumnName="APA_ID")
     * @ORM\JoinColumn(
     *     nullable=false,
     * )
     * @Assert\NotNull(
     *     message="Un fichier doit obligatoirement être relié à une Page."
     * )
     */
    private $page;

    /**
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Etagère"}
     * )
     * @Assert\NotBlank(
     *     message="Le numéro d'étagère est obligatoire."
     * )
     * @JMS\Groups({"list","details"})
     */
    private $shelf;

    /**
     * @ORM\Column(
     *     type="string",
     *      length=255,
     *      options={"comment":"Prénom"}
     * )
     * @Assert\NotBlank(
     *     message="Le prénom est obligatoire."
     * )
     * @JMS\Groups({"list","details"})
     */
    private $firstname;

    /**
     * @ORM\Column(
     *     type="string",
     *      length=255,
     *     options={"comment":"Nom de famille"}
     * )
     * @Assert\NotBlank(
     *     message="Le nom de famille est obligatoire."
     * )
     * @JMS\Groups({"list","details"})
     */
    private $lastname;

    /**
     * @ORM\Column(
     *     type="date",
     *     options={"comment":"Date de naissance"}
     * )
     * @Assert\NotNull(
     *     message="La date de naissance est obligatoire."
     * )
     * @JMS\Groups({"list","details"})
     */
    private $dob;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FileCategory")
     * @ORM\JoinColumn(
     *     name="AFC_ID",
     *     referencedColumnName="AFC_ID",
     * )
     * @Assert\NotNull(
     *     message="Un dossier doit obligatoirement avoir une catégorie."
     * )
     */
    private $category;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=true,
     *     options={"comment":"Nom d'usage"}
     * )
     * @JMS\Groups({"list","details"})
     */
    private $commonName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Box", inversedBy="files")
     * @ORM\JoinColumn(
     *     name="ABO_ID",
     *     referencedColumnName="ABO_ID",
     *     onDelete="SET NULL",
     * )
     */
    private $box;

    public static function getTrigram(): string
    {
        return "afi";
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Page|null
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page|null $page
     * @return $this
     */
    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getShelf(): ?int
    {
        return $this->shelf;
    }

    /**
     * @param int $shelf
     * @return $this
     */
    public function setShelf(int $shelf): self
    {
        $this->shelf = $shelf;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return $this
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDob(): ?DateTimeInterface
    {
        return $this->dob;
    }

    /**
     * @param DateTimeInterface $dob
     * @return $this
     */
    public function setDob(DateTimeInterface $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("category")
     * @JMS\Groups({"list","details"})
     *
     * @return string
     */
    public function getStringCategory(): ?string
    {
        return FileService::toStringCategory($this);
    }

    /**
     * @return FileCategory|null
     */
    public function getCategory(): ?FileCategory
    {
        return $this->category;
    }

    /**
     * @param FileCategory|null $category
     * @return $this
     */
    public function setCategory(?FileCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Box|null
     */
    public function getBox(): ?Box
    {
        return $this->box;
    }

    /**
     * @param Box|null $box
     * @return $this
     */
    public function setBox(?Box $box): self
    {
        $this->box = $box;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return str_pad($this->getNumber(), 5, ' ',
                STR_PAD_LEFT) . ' : ' . $this->getCommonName() . ' ' . $this->getFirstname();
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int|null $number
     * @return $this
     */
    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCommonName(): ?string
    {
        return $this->commonName;
    }

    /**
     * @param string|null $commonName
     * @return $this
     */
    public function setCommonName(?string $commonName): self
    {
        $this->commonName = $commonName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return $this
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }
}
