<?php

namespace App\Entity;

use App\Constraint\CanCreateBoxConstraint;
use App\NamingStrategy\Trigramed;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoxRepository")
 * @CanCreateBoxConstraint()
 * UniqueEntity(
 *     fields={"number", "lot"},
 *     errorPath="number",
 *     message="Un carton avec ce numéro existe déjà pour ce lot."
 * )
 */
class Box implements Trigramed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Identifiant technique"}
     * )
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Numéro"}
     * )
     * @JMS\Groups({"details"})
     */
    private $number;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\File", mappedBy="box", cascade={"persist"})
     * @ORM\OrderBy({"commonName"="ASC","firstname"="ASC"})
     */
    private $files;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lot", inversedBy="boxes")
     * @ORM\JoinColumn(nullable=false,name="ALO_ID", referencedColumnName="ALO_ID")
     */
    private $lot;

    /**
     * Box constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    public static function getTrigram(): string
    {
        return "abo";
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setBox($this);
        }

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("nbFiles")
     * @JMS\Groups({"details"})
     *
     * @return int
     */
    public function getNbFiles(): int
    {
        return $this->getFiles()->count();
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
            // set the owning side to null (unless already changed)
            if ($file->getBox() === $this) {
                $file->setBox(null);
            }
        }

        return $this;
    }

    /**
     * @return Lot|null
     */
    public function getLot(): ?Lot
    {
        return $this->lot;
    }

    /**
     * @param Lot|null $lot
     * @return $this
     */
    public function setLot(?Lot $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (is_null($this->getFirstFile()) ? '' : $this->getFirstFile()) . ' => ' . (is_null($this->getLastFile()) ? '' : $this->getLastFile());
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("firstFile")
     * @JMS\Groups({"details"})
     *
     * @return File|null
     */
    public function getFirstFile(): ?File
    {
        return $this->getFiles()->isEmpty() ? null : $this->getFiles()->first();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("lastFile")
     * @JMS\Groups({"withLast"})
     *
     * @return File|null
     */
    public function getLastFile(): ?File
    {
        return $this->getFiles()->isEmpty() ? null : $this->getFiles()->last();
    }
}
