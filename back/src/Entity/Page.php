<?php

namespace App\Entity;

use App\NamingStrategy\Trigramed;
use Countable;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 * @UniqueEntity(
 *     fields={"number", "lot"},
 *     errorPath="number",
 *     message="Une page avec ce numéro existe déjà pour ce lot."
 * )
 */
class Page implements Countable, Trigramed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Identifiant technique"}
     * )
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Numéro"}
     * )
     * @JMS\Groups({"list","details"})
     */
    private $number;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     nullable=true,
     *     options={"comment":"Date de validation"}
     * )
     * @JMS\Groups({"list","details"})
     */
    private $validatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\File", mappedBy="page", orphanRemoval=true, cascade={"persist"})
     * @JMS\Groups({"withFiles"})
     * @ORM\OrderBy({"number" = "ASC"})
     * @JMS\Type("ArrayCollection<App\Entity\File>")
     */
    private $files;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lot", inversedBy="pages", inversedBy="number")
     * @ORM\JoinColumn(name="ALO_ID",nullable=false, referencedColumnName="ALO_ID")
     * @JMS\Exclude()
     */
    private $lot;

    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    public static function getTrigram(): string
    {
        return "apa";
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getValidatedAt(): ?DateTimeInterface
    {
        return $this->validatedAt;
    }

    /**
     * @param DateTimeInterface|null $validatedAt
     * @return $this
     */
    public function setValidatedAt(?DateTimeInterface $validatedAt): self
    {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setPage($this);
        }

        return $this;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
            // set the owning side to null (unless already changed)
            if ($file->getPage() === $this) {
                $file->setPage(null);
            }
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return $this->getFiles()->count();
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @throws \Exception
     */
    public function validate()
    {
        $this->setValidatedAt(new DateTime());
    }

    /**
     * @Assert\IsTrue(
     *     message="Impossible de classer les fichiers d'un Lot en dehors de la phase de classement."
     * )
     */
    public function isInPhase(): bool
    {
        return "classification" === $this->getLot()->getState();
    }

    /**
     * @return Lot|null
     */
    public function getLot(): ?Lot
    {
        return $this->lot;
    }

    /**
     * @param Lot|null $lot
     * @return $this
     */
    public function setLot(?Lot $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @Assert\IsTrue(
     *     message="Vous devez imprimer les étiquettes d'étagère avant de modifier le classement d'une page."
     * )
     */
    public function isEditable(): bool
    {
        return $this->getLot()->getLastShelfExportedAt() instanceof DateTime;
    }
}
