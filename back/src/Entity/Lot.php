<?php

namespace App\Entity;

use App\NamingStrategy\Trigramed;
use App\Repository\PageRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LotRepository")
 * @UniqueEntity(
 *     fields={"number"},
 *     errorPath="number",
 *     message="Un Lot existe déjà avec ce numéro."
 * )
 *
 */
class Lot implements Trigramed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Identifiant technique"}
     * )
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Numéro"}
     * )
     * @JMS\Groups({"list","details", "submission"})
     * @Assert\NotNull(
     *     message="Le numéro de Lot est obligatoire."
     * )
     */
    private $number;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=20,
     *     options={"comment":"Etat d'avancement"}
     * )
     * @JMS\Groups({"list", "details"})
     */
    private $state;

    /**
     * @ORM\Column(
     *     type="integer",
     *     options={"comment":"Nombre de Dossiers"}
     * )
     * @JMS\Groups({"list","details"})
     */
    private $nbFiles;

    /**
     * @ORM\Column(
     *     type="boolean",
     *     options={"comment":"Avec Nir"}
     * )
     * @JMS\Groups({"details", "submission"})
     * @Assert\NotNull(
     *     message="L'information avec ou sans NIR est obligatoire."
     * )
     */
    private $withNir;

    /**
     * @ORM\Column(
     *     type="boolean",
     *     options={"comment":"Avec Affiliation"}
     * )
     * @JMS\Groups({"details", "submission"})
     * @Assert\NotNull(
     *     message="L'information avec ou sans Affiliation est obligatoire."
     * )
     */
    private $withAffiliation;

    /**
     * @ORM\Column(
     *     type="date",
     *     options={"comment":"Borne de début"}
     * )
     * @Assert\NotNull()
     * @JMS\Groups({"details", "submission"})
     * @Assert\GreaterThan(
     *     "1900-01-01",
     *     message="La date de début de doit être supérieur à 01/01/1900."
     * )
     */
    private $dobFrom;

    /**
     * @ORM\Column(
     *     type="date",
     *     options={"comment":"Borne de fin"}
     * )
     * @Assert\NotNull()
     * @JMS\Groups({"details", "submission"})
     * @Assert\GreaterThan(
     *     propertyPath="dobFrom",
     *     message="La date de fin doit être supérieur à la date de début."
     * )
     */
    private $dobTo;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     options={"comment":"Date de création"}
     * )
     * @JMS\Groups({"details"})
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Page", mappedBy="lot", orphanRemoval=true, cascade={"persist"})
     * @JMS\Exclude()
     */
    private $pages;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     nullable=true,
     *     options={"comment":"Date de fin de classement"}
     * )
     * @JMS\Groups({"details"})
     */
    private $submitedAt;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     nullable=true,
     *     options={"comment":"Date de fin de mise en carton"}
     * )
     * @JMS\Groups({"details"})
     */
    private $sentAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Box", mappedBy="lot", orphanRemoval=true)
     */
    private $boxes;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     nullable=true,
     *     options={"comment":"Derniere date d'export d'étagère"}
     * )
     * @JMS\Groups({"details"})
     */
    private $lastShelfExportedAt;

    /**
     * Lot constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
        $this->pages = new ArrayCollection();
        $this->setNbFiles(0);
        $this->boxes = new ArrayCollection();
        $this->setWithAffiliation(false);
        $this->setWithNir(false);
    }

    public static function getTrigram(): string
    {
        return "alo";
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("title")
     * @JMS\Groups({"list","details"})
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return self::generateTitle($this);
    }

    /**
     * Generate the title of a lot
     * @param Lot $lot
     * @return string
     */
    public static function generateTitle(self $lot): string
    {
        return "Dossiers " .
            ($lot->getWithNir() ? 'avec' : 'sans') . ' NIR ' .
            ($lot->getWithAffiliation() ? 'avec' : 'sans') . ' Affiliation' .
            ($lot->getDobFrom() instanceof DateTime ? ' de ' . $lot->getDobFrom()->format('d/m/Y') : '') .
            ($lot->getDobTo() instanceof DateTime ? ' à ' . $lot->getDobTo()->format('d/m/Y') : '');
    }

    /**
     * @return bool|null
     */
    public function getWithNir(): ?bool
    {
        return $this->withNir;
    }

    /**
     * @param bool $withNir
     * @return $this
     */
    public function setWithNir(bool $withNir): self
    {
        $this->withNir = $withNir;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getWithAffiliation(): ?bool
    {
        return $this->withAffiliation;
    }

    /**
     * @param bool $withAffiliation
     * @return $this
     */
    public function setWithAffiliation(bool $withAffiliation): self
    {
        $this->withAffiliation = $withAffiliation;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDobFrom(): ?DateTimeInterface
    {
        return $this->dobFrom;
    }

    /**
     * @param DateTimeInterface|null $dobFrom
     * @return $this
     */
    public function setDobFrom(?DateTimeInterface $dobFrom): self
    {
        $this->dobFrom = $dobFrom;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDobTo(): ?DateTimeInterface
    {
        return $this->dobTo;
    }

    /**
     * @param DateTimeInterface|null $dobTo
     * @return $this
     */
    public function setDobTo(?DateTimeInterface $dobTo): self
    {
        $this->dobTo = $dobTo;

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("nbPages")
     * @JMS\Groups({"details"})
     *
     * @return string
     */
    public function getNbPages(): int
    {
        return $this->getPages()->count();
    }

    /**
     * @return Collection
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("nbValidatedPages")
     * @JMS\Groups({"details"})
     *
     * @return string
     */
    public function getNbValidatedPages(): int
    {
        return $this->pages->matching(PageRepository::getValidatedPageCriteria())->count();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("nbBoxes")
     * @JMS\Groups({"details"})
     *
     * @return string
     */
    public function getNbBox()
    {
        return $this->getBoxes()->count();
    }

    /**
     * @return Collection|Box[]
     */
    public function getBoxes(): Collection
    {
        return $this->boxes;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("nbFilesInBoxes")
     * @JMS\Groups({"details"})
     *
     * @return string
     */
    public function getNbFilesInBoxes()
    {
        return array_sum($this->getBoxes()->map(function (Box $box) {
            return $box->getNbFiles();
        })->toArray());
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return $this
     */
    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @param Page $page
     * @return $this
     */
    public function addPage(Page $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setLot($this);
            $this->setNbFiles($this->getNbFiles() + count($page));
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNbFiles(): ?int
    {
        return $this->nbFiles;
    }

    /**
     * @param int $nbFiles
     * @return $this
     */
    public function setNbFiles(int $nbFiles): self
    {
        $this->nbFiles = $nbFiles;

        return $this;
    }

    /**
     * Get the last validated Page of the Lot
     * @JMS\VirtualProperty
     * @JMS\SerializedName("lastValidatedPage")
     * @JMS\Groups({"details"})
     *
     * @return Page|null
     */
    public function getLastValidatedPage(): ?Page
    {
        $page = $this->pages->matching(PageRepository::getLastValidated())->first();
        if ($page instanceof Page) {
            return $page;
        } else {
            return null;
        }
    }

    /**
     * Get the First not validated Page of the Lot
     * @JMS\VirtualProperty
     * @JMS\SerializedName("firstNotValidatedPage")
     * @JMS\Groups({"details"})
     *
     * @return Page|null
     */
    public function getFirstNotValidatedPage(): ?Page
    {
        $page = $this->pages->matching(PageRepository::getFirstNotValidated())->first();
        if ($page instanceof Page) {
            return $page;
        } else {
            return null;
        }
    }

    /**
     * @param Page $page
     * @return $this
     */
    public function removePage(Page $page): self
    {
        if ($this->pages->contains($page)) {
            $this->pages->removeElement($page);
            // set the owning side to null (unless already changed)
            if ($page->getLot() === $this) {
                $page->setLot(null);
            }
            $this->setNbFiles($this->getNbFiles() - count($page));
        }

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getSubmitedAt(): ?DateTimeInterface
    {
        return $this->submitedAt;
    }

    /**
     * @param DateTimeInterface|null $submitedAt
     * @return $this
     */
    public function setSubmitedAt(?DateTimeInterface $submitedAt): self
    {
        $this->submitedAt = $submitedAt;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getSentAt(): ?DateTimeInterface
    {
        return $this->sentAt;
    }

    /**
     * @param DateTimeInterface|null $sentAt
     * @return $this
     */
    public function setSentAt(?DateTimeInterface $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * @param Box $box
     * @return $this
     */
    public function addBox(Box $box): self
    {
        if (!$this->boxes->contains($box)) {
            $this->boxes[] = $box;
            $box->setLot($this);
        }

        return $this;
    }

    /**
     * @param Box $box
     * @return $this
     */
    public function removeBox(Box $box): self
    {
        if ($this->boxes->contains($box)) {
            $this->boxes->removeElement($box);
            // set the owning side to null (unless already changed)
            if ($box->getLot() === $this) {
                $box->setLot(null);
            }
        }

        return $this;
    }

    public function getLastShelfExportedAt(): ?\DateTimeInterface
    {
        return $this->lastShelfExportedAt;
    }

    public function setLastShelfExportedAt(?\DateTimeInterface $lastShelfExportedAt): self
    {
        $this->lastShelfExportedAt = $lastShelfExportedAt;

        return $this;
    }
}
