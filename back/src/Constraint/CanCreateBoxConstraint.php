<?php


namespace App\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CanCreateBoxConstraint extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Impossible de créer un carton en dehors de la phase prévu à cet effet.';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return \get_class($this) . 'Validator';
    }

    /**
     * @return array|string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}