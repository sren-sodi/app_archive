<?php


namespace App\Constraint;


use App\Entity\Box;
use App\Service\Entity\BoxService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class CanCreateBoxConstraintValidator
 * @package App\Constraint
 */
class CanCreateBoxConstraintValidator extends ConstraintValidator
{
    /**
     * @var BoxService
     */
    private $boxService;

    /**
     * CanCreateBoxConstraintValidator constructor.
     * @param BoxService $boxService
     */
    public function __construct(BoxService $boxService)
    {
        $this->boxService = $boxService;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CanCreateBoxConstraint) {
            throw new UnexpectedTypeException($constraint, CanCreateBoxConstraint::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (false === $value instanceof Box) {
            throw new UnexpectedTypeException($value, Box::class);
        }

        $canCreate = $this->boxService->canCreateBox($value);

        if (false === $canCreate) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}