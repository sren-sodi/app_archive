<?php


namespace App\Helper;


use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

trait LoggerAwareTrait
{
    /**
     * @var LoggerInterface $logger The logger instance in use.
     */
    private $logger;


    /**
     * Set the logger to be used by this class.
     *
     * @param LoggerInterface $logger The logger to use
     * @required
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;

        return $this;
    }

    public function debug(string $msg, array $context = array())
    {
        $this->getLogger()->debug($msg, $context);
    }

    /**
     * Get the logger currently in use.
     *
     * @return LoggerInterface
     */
    private function getLogger(): LoggerInterface
    {
        if ($this->logger === null) {
            $this->logger = new NullLogger();
        }

        return $this->logger;
    }
}