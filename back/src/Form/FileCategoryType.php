<?php

namespace App\Form;

use App\DataTransformer\FileCategoryTransformer;
use App\DataTransformer\FileCategoryViewTransformer;
use App\Entity\FileCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FileCategoryType
 * @package App\Form
 */
class FileCategoryType extends AbstractType
{
    /**
     * @var FileCategoryTransformer
     */
    private $fileCategoryTransformer;

    /**
     * @var FileCategoryViewTransformer
     */
    private $fileCategoryViewTransformer;

    /**
     * FileCategoryType constructor.
     * @param FileCategoryTransformer $fileCategoryTransformer
     * @param FileCategoryViewTransformer $fileCategoryViewTransformer
     */
    public function __construct(
        FileCategoryTransformer $fileCategoryTransformer,
        FileCategoryViewTransformer $fileCategoryViewTransformer
    ) {
        $this->fileCategoryTransformer = $fileCategoryTransformer;
        $this->fileCategoryViewTransformer = $fileCategoryViewTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->fileCategoryTransformer);
        $builder->addViewTransformer($this->fileCategoryViewTransformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => FileCategory::class,
            ]
        );
    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return TextType::class;
    }


}
