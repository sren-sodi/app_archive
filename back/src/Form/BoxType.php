<?php

namespace App\Form;

use App\Entity\Box;
use App\Entity\File;
use App\Service\Entity\BoxService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BoxType
 * @package App\Form
 */
class BoxType extends AbstractType
{
    /**
     * @var BoxService
     */
    private $boxService;

    /**
     * BoxType constructor.
     * @param BoxService $boxService
     */
    public function __construct(BoxService $boxService)
    {
        $this->boxService = $boxService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastFile', SelectFileType::class, [
                'mapped' => false,
                'required' => true,
            ]);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $box = $event->getData();
            $form = $event->getForm();
            $limitFile = $event->getForm()->get('lastFile')->getData();
            if ($form->isValid()) {
                if ($limitFile instanceof File && $box instanceof Box) {
                    $this->boxService->setFiles($box, $limitFile);
                } else {
                    $form->addError(new FormError("LastFile is not a File"));
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Box::class,
        ]);
    }
}
