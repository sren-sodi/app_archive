<?php

namespace App\Form;

use App\Entity\Page;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PageType
 * @package App\Form
 */
class PageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'files',
                CollectionType::class,
                [
                    'entry_type' => FileType::class,
                ]
            );

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $formEvent) {
                $this->saveValidationDate($formEvent);
            }
        );
    }

    /**
     * Save Validation date is form is valid
     * @param FormEvent $formEvent
     */
    private function saveValidationDate(FormEvent $formEvent)
    {
        $page = $formEvent->getData();
        $form = $formEvent->getForm();
        /**
         * @var Page $page
         */
        if ($form->isValid()) {
            $page->validate();
        }
        $formEvent->setData($page);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Page::class,
            ]
        );
    }
}
