<?php

namespace App\Form;

use App\Entity\Lot;
use App\Service\Entity\LotService;
use App\Service\Transverse\PensionService;
use Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class LotType
 * @package App\Form
 */
class LotType extends AbstractType
{
    /**
     * @var PensionService
     */
    private $pensionService;

    /**
     * LotType constructor.
     * @param PensionService $pensionService
     */
    public function __construct(PensionService $pensionService)
    {
        $this->pensionService = $pensionService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number')
            ->add(
                'withNir',
                CheckboxType::class
            )
            ->add(
                'withAffiliation',
                CheckboxType::class
            )
            ->add(
                'dobFrom',
                DateType::class,
                [
                    // date are sent as a string and not as an array
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'dobTo',
                DateType::class,
                [
                    'widget' => 'single_text',
                ]
            );

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($options) {
                if (true === $options['estimate_only']) {
                    $this->countFiles($event);
                } else {
                    $this->setFiles($event);
                }
            }
        );
    }

    /**
     * @param FormEvent $event
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @todo test
     */
    private function countFiles(FormEvent $event)
    {
        /**
         * @var Lot $data
         */
        $data = $event->getData();
        $form = $event->getForm();

        if ($form->isValid()) {
            try {
                $count = $this->pensionService->countFilesForLot($data);
                $data->setNbFiles($count);
                $event->setData($data);
            } catch (Exception $e) {
                $form->addError(self::createFormError($e->getMessage()));
            }
        }
    }

    /**
     * @param string $message
     * @return FormError
     */
    private static function createFormError(string $message): FormError
    {
        return new FormError($message);
    }

    /**
     * @param FormEvent $event
     * @throws TransportExceptionInterface
     * @todo test
     * Before persist, get files from PENSION
     */
    private function setFiles(FormEvent $event)
    {
        /**
         * @var Lot $data
         */
        $data = $event->getData();
        $form = $event->getForm();

        if ($form->isValid()) {
            try {
                $files = $this->pensionService->getFilesForLot($data);
                $data = LotService::addFiles($data, $files, 20);
                $event->setData($data);
            } catch (Exception $e) {
                $form->addError(self::createFormError($e->getMessage()));
            }
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Lot::class,
                'estimate_only' => false,
            ]
        );
        $resolver->setAllowedTypes('estimate_only', 'bool');
    }
}
