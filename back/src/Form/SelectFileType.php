<?php


namespace App\Form;


use App\DataTransformer\FileTransformer;
use App\DataTransformer\FileViewTransformer;
use App\Entity\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SelectFileType
 * @package App\Form
 */
class SelectFileType extends AbstractType
{
    /**
     * @var FileTransformer
     */
    private $fileTransformer;

    /**
     * @var FileViewTransformer
     */
    private $fileViewTransformer;

    /**
     * SelectFileType constructor.
     * @param FileTransformer $fileTransformer
     * @param FileViewTransformer $fileViewTransformer
     */
    public function __construct(FileTransformer $fileTransformer, FileViewTransformer $fileViewTransformer)
    {
        $this->fileTransformer = $fileTransformer;
        $this->fileViewTransformer = $fileViewTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->fileTransformer);
        $builder->addViewTransformer($this->fileViewTransformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => File::class,
                'invalid_message' => "lol",
            ]
        );
    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return NumberType::class;
    }


}
