<?php


namespace App\EventListener;


use App\Entity\Lot;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Workflow\Workflow;

/**
 * Class LotWorkflowStarting
 * @package App\EventListener
 */
class LotWorkflowStarting
{
    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * LotWorkflowStarting constructor.
     * @param Workflow $workflow
     */
    public function __construct(Workflow $workflow)
    {
        $this->workflow = $workflow;
    }

    /**
     * @param Lot $lot
     * @param LifecycleEventArgs $event
     */
    public function prePersist(Lot $lot, LifecycleEventArgs $event)
    {
        if (true === empty($lot->getState())) {
            $lot->setState(current($this->workflow->getDefinition()->getInitialPlaces()));
        }
    }
}