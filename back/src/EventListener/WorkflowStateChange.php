<?php


namespace App\EventListener;


use App\Entity\Box;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Workflow\Workflow;

/**
 * Class WorkflowStateChange
 * @package App\EventListener
 */
class WorkflowStateChange
{
    const BOX_PACKING_TRANSITION = 'submit';
    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * WorkflowStateChange constructor.
     * @param Workflow $workflow
     */
    public function __construct(Workflow $workflow)
    {
        $this->workflow = $workflow;
    }

    /**
     * @param Box $box
     * @param LifecycleEventArgs $event
     */
    public function prePersist(Box $box, LifecycleEventArgs $event)
    {
        $lot = $box->getLot();
        $manager = $event->getObjectManager();
        if ($this->workflow->can($lot, self::BOX_PACKING_TRANSITION)) {
            $this->workflow->apply($lot, self::BOX_PACKING_TRANSITION);
        }
        $manager->persist($lot);
    }
}