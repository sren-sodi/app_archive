<?php

namespace App\Controller;

use App\Entity\Lot;
use App\Exception\FormErrorException;
use App\Form\LotType;
use App\Service\Entity\LotService;
use Doctrine\ORM\NonUniqueResultException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Workflow\Transition;

/**
 * @Rest\RouteResource("Lot")
 * @SWG\Tag(name="Lot")
 */
class LotController extends AbstractFOSRestController
{
    private $lotService;

    /**
     * LotController constructor.
     * @param LotService $lotService
     */
    public function __construct(LotService $lotService)
    {
        $this->lotService = $lotService;
    }

    /**
     * Get the next Lot
     * @Rest\View(serializerGroups={"submission"})
     * @return Lot
     * @throws NonUniqueResultException
     */
    public function newAction()
    {
        return $this->lotService->createNew();
    }

    /**
     * Estimate a Lot
     * @Rest\Post("/lots/estimate")
     * @SWG\Response(response=200,
     *     description="The number of file that would belond to the lot",
     *     @SWG\Schema(type="integer"))
     * )
     * @SWG\Parameter(name="lot",
     *     in="body",
     *     description="The lot to estimate",
     *     @SWG\Schema(ref=@Model(type=LotType::class))
     * )
     * @SWG\Response(response=400,description="An error occured")
     */
    public function postEstimateAction(Request $request)
    {
        $context = new Context();
        try {
            $lot = $this->lotService->createAndEstimateLot($request->request->all());
            $context->addGroup('details');
            $view = $this->view($lot, Response::HTTP_CREATED);
        } catch (FormErrorException $e) {
            $view = $this->view($e->getFormError(), Response::HTTP_BAD_REQUEST);
        }
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * Get the workflow available transitions of a Lot
     * @SWG\Response(response=200,
     *     description="Returns the list of transition the lot can use"
     * )
     * @SWG\Response(response=404,
     *     description="The lot does not exist"
     * )
     * @Rest\View()
     * @param Lot $lot
     * @return array|false
     */
    public function getTransitionAction(Lot $lot)
    {
        return array_map(
            function (Transition $transition) {
                return current($transition->getTos());
            },
            $this->lotService->getTransitions($lot)
        );
    }

    /**
     * Get a specific Lot
     * @SWG\Response(response=200,
     *     description="Returns a lot",
     *     @SWG\Schema(ref=@Model(type=Lot::class, groups={"details"}))
     * )
     * @SWG\Response(response=404,
     *     description="The lot does not exist"
     * )
     * @SWG\Parameter(name="lot",
     *     in="path",
     *     type="number",
     *     description="The number of the lot"
     * )
     * @Rest\View(serializerGroups={"details", "last_validated_page"={"list"}})
     * @param Lot $lot
     * @return Lot
     */
    public function getAction(Lot $lot)
    {
        return $lot;
    }


    /**
     * Get a list of Lot based on filters
     * @SWG\Response(response=200,
     *     description="Returns a list of lot",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Lot::class, groups={"list"}))
     *     )
     * )
     * @SWG\Parameter(name="offset",
     *     in="query",
     *     type="number",
     *     description="The offset of the list"
     * )
     * @SWG\Parameter(name="limit",
     *     in="query",
     *     type="number",
     *     description="The maximum number of lot"
     * )
     * @param ParamFetcher $paramFetcher
     *
     * @QueryParam(name="limit", requirements="\d+", strict=true, nullable=true, description="The maximum number of lot")
     * @QueryParam(name="offset", requirements="\d+", strict=true, nullable=true, description="The offset of the list")
     * @return Response
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $view = $this->view($this->lotService->getLots($paramFetcher->get('limit'), $paramFetcher->get('offset')));
        $context = new Context();
        $context->addGroup('list');
        $view->setContext($context);
        $view->setHeader('X-Total-Count', $this->lotService->countLots());

        return $this->handleView($view);
    }

    /**
     * Create a new Lot
     * @SWG\Response(response=201,
     *     description="The lot was created",
     * )
     * @SWG\Response(response=400,
     *     description="An error occured",
     *     @SWG\Schema(ref=@Model(type=LotType::class))
     * )
     * @SWG\Parameter(name="lot",
     *     in="body",
     *     description="The lot to create",
     *     @SWG\Schema(ref=@Model(type=LotType::class))
     * )
     */
    public function postAction(Request $request)
    {
        $context = new Context();
        try {
            $lot = $this->lotService->createAndSaveLot($request->request->all());
            $context->addGroup('details');
            $view = $this->view($lot, Response::HTTP_CREATED);
        } catch (FormErrorException $e) {
            $view = $this->view($e->getFormError(), Response::HTTP_BAD_REQUEST);
        }
        $view->setContext($context);

        return $this->handleView($view);
    }
}
