<?php

namespace App\Controller;

use App\Entity\Box;
use App\Entity\Lot;
use App\Exception\FileNotFoundException;
use App\Exception\FormErrorException;
use App\Form\BoxType;
use App\Service\Entity\BoxService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\RouteResource("Box")
 * @SWG\Tag(name="Box")
 */
class BoxController extends AbstractFOSRestController
{
    private $boxService;

    public function __construct(BoxService $boxService)
    {
        $this->boxService = $boxService;
    }

    /**
     * Get the next Box to create
     * @SWG\Response(response=200,
     *     description="The box to create",
     *     @SWG\Schema(ref=@Model(type=Box::class, groups={"details","first_file"={"list"}}))
     * )
     * @Rest\View(serializerGroups={"details", "first_file"={"list"}})
     * @param Lot $lot
     * @return Box
     * @throws FileNotFoundException
     * @throws NonUniqueResultException
     */
    public function newAction(Lot $lot)
    {
        return $this->boxService->initiateFromLot($lot);
    }

    /**
     * Get a specific Box
     * @SWG\Response(response=200,
     *     description="The box with with the given number",
     *     @SWG\Schema(ref=@Model(type=Box::class, groups={"details","first_file"={"list"}}))
     * )
     * @Rest\View(serializerGroups={"details", "first_file"={"list"},"withLast", "last_file"={"list"}})
     * @param Lot $lot
     * @param Box $box
     * @return Box
     * @throws FileNotFoundException
     * @throws NonUniqueResultException
     */
    public function getAction(Lot $lot, Box $box)
    {
        return $box;
    }

    /**
     * Create the next box for the given Lot
     * @SWG\Parameter(name="lastFile",
     *     in="body",
     *     description="The last file of the new Box",
     *     @SWG\Schema(ref=@Model(type=BoxType::class))
     * )
     * @SWG\Response(response=200,
     *     description="The box with with the given number",
     *     @SWG\Schema(ref=@Model(type=Box::class, groups={"details","first_file"={"list"}}))
     * )
     * @SWG\Tag(name="Box")
     * @param Lot $lot
     * @param Request $request
     * @return Response
     * @throws FileNotFoundException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function postAction(Lot $lot, Request $request)
    {
        $context = new Context();
        try {
            $box = $this->boxService->createNewBox($lot, $request->request->all());
            $context->addGroup('details');
            $context->addGroup('withLast');
            $view = $this->view($box, Response::HTTP_CREATED);
        } catch (FormErrorException $e) {
            $view = $this->view($e->getFormError(), Response::HTTP_BAD_REQUEST);

        }
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * Update a box and remove the next boxes of the given Lot
     * @SWG\Parameter(name="lastFile",
     *     in="body",
     *     description="The last file of the new Box",
     *     @SWG\Schema(ref=@Model(type=BoxType::class))
     * )
     * @SWG\Response(response=201,
     *     description="The updated box with the given number",
     *     @SWG\Schema(ref=@Model(type=Box::class, groups={"details","first_file"={"list"},"last_file"={"list"}}))
     * )
     * @SWG\Tag(name="Box")
     * @param Lot $lot
     * @param Box $box
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function putAction(Lot $lot, Box $box, Request $request)
    {
        $context = new Context();
        try {
            $box = $this->boxService->editAndRemoveNextBoxes($box, $request->request->all());
            $context->addGroup('details');
            $context->addGroup('withLast');
            $view = $this->view($box, Response::HTTP_OK);
        } catch (FormErrorException $e) {
            $view = $this->view($e->getFormError(), Response::HTTP_BAD_REQUEST);
        }
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * Estimate the next box for the given Lot
     * @Rest\Post("/lots/{lot}/boxes/estimate")
     * @SWG\Parameter(name="lastFile",
     *     in="body",
     *     description="The last file of the new Box",
     *     @SWG\Schema(ref=@Model(type=BoxType::class))
     * )
     * @SWG\Response(response=200,
     *     description="The box with with the given number",
     *     @SWG\Schema(ref=@Model(type=Box::class, groups={"details","first_file"={"list"}, "last_file"={"list"}}))
     * )
     * @SWG\Tag(name="Box")
     * @param Lot $lot
     * @param Request $request
     * @return Response
     * @throws FileNotFoundException
     * @throws NonUniqueResultException
     */
    public function estimateAction(Lot $lot, Request $request)
    {
        $context = new Context();
        try {
            $box = $this->boxService->estimateNew($lot, $request->request->all());
            $context->addGroup('details');
            $context->addGroup('withLast');
            $view = $this->view($box, Response::HTTP_CREATED);
        } catch (FormErrorException $e) {
            $view = $this->view($e->getFormError(), Response::HTTP_BAD_REQUEST);

        }
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * Estimate the given box for the given Lot
     * @Rest\Post("/lots/{lot}/boxes/{box}/estimate")
     * @SWG\Parameter(name="lastFile",
     *     in="body",
     *     description="The last file of the new Box",
     *     @SWG\Schema(ref=@Model(type=BoxType::class))
     * )
     * @SWG\Response(response=200,
     *     description="The box with with the given number",
     *     @SWG\Schema(ref=@Model(type=Box::class, groups={"details","first_file"={"list"}, "last_file"={"list"}}))
     * )
     * @SWG\Tag(name="Box")
     * @param Lot $lot
     * @param Box $box
     * @param Request $request
     * @return Response
     */
    public function estimateBoxAction(Lot $lot, Box $box, Request $request)
    {
        $context = new Context();
        try {
            $box = $this->boxService->estimateBox($box, $request->request->all());
            $context->addGroup('details');
            $context->addGroup('withLast');
            $view = $this->view($box, Response::HTTP_CREATED);
        } catch (FormErrorException $e) {
            $view = $this->view($e->getFormError(), Response::HTTP_BAD_REQUEST);

        }
        $view->setContext($context);

        return $this->handleView($view);
    }
}
