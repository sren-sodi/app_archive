<?php

namespace App\Controller;

use App\Entity\Lot;
use App\Entity\Page;
use App\Exception\FormErrorException;
use App\Form\PageType;
use App\Service\Entity\PageService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\RouteResource("Page")
 * @SWG\Tag(name="Page")
 */
class PageController extends AbstractFOSRestController
{
    private $pageService;

    public function __construct(PageService $pageService)
    {
        $this->pageService = $pageService;
    }

    /**
     * Get a specific Page
     * @SWG\Response(response=200,
     *     description="Returns a page",
     *     @SWG\Schema(ref=@Model(type=Page::class,groups={"details"}))
     * )
     * @SWG\Response(response=404,
     *     description="The page does not exist"
     * )
     * @Rest\View(serializerGroups={"details", "withFiles"})
     * @param Lot $lot
     * @param Page $page
     * @return Page
     */
    public function getAction(Lot $lot, Page $page)
    {
        return $page;
    }

    /**
     * Submit a modified Page
     * @param Lot $lot
     * @param Page $page
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     * @todo
     * @SWG\Response(response=201,
     *     description="The page has been submited",
     * )
     * @SWG\Parameter(name="files",
     *     in="body",
     *     description="The list of files to validate",
     *     @SWG\Schema(ref=@Model(type=PageType::class))
     * )
     * @SWG\Response(response=400,description="An error occured")
     * @Rest\View(serializerGroups={"details",  "withFiles"})
     */
    public function patchAction(Lot $lot, Page $page, Request $request)
    {
        $context = new Context();
        try {
            $page = $this->pageService->submit($page, $request->request->all());
            $context->addGroup('details');
            $context->addGroup("withFiles");
            $view = $this->view($page, Response::HTTP_CREATED);
        } catch (FormErrorException $e) {
            $view = $this->view($e->getFormError(), Response::HTTP_BAD_REQUEST);
        }
        $view->setContext($context);

        return $this->handleView($view);
    }

}
