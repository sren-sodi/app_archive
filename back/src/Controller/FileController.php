<?php

namespace App\Controller;

use App\Entity\File;
use App\Entity\Lot;
use App\Exception\BoxNotFoundException;
use App\Service\Entity\BoxService;
use App\Service\Entity\FileService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Rest\RouteResource("File")
 * @SWG\Tag(name="File")
 */
class FileController extends AbstractFOSRestController
{
    const EXPORT_SHELF_TITLE = 'ARCHIVAGE-etiquettes_etageres-%s.csv';
    private $fileService;
    private $boxService;

    public function __construct(FileService $fileService, BoxService $boxService)
    {
        $this->fileService = $fileService;
        $this->boxService = $boxService;
    }

    /**
     * Returns a list a File to build or update a Box according to filters
     * @param Lot $lot
     * @param ParamFetcher $paramFetcher
     * @return array
     * @SWG\Response(response=200,
     *     description="Return the number of file of the given lot according to filters",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=File::class, groups={"list"}))
     *          )
     * )
     * @SWG\Response(response=404,
     *     description="The lot does not exist"
     * )
     * @Rest\View(serializerGroups={"list"})
     * @QueryParam(name="firstname", requirements="\w*", strict=true, nullable=true, description="filter on the firstname", default="")
     * @QueryParam(name="commonName", requirements="\w*", strict=true, nullable=true, description="filter on the commoneName", default="")
     * @QueryParam(name="dob", strict=true, nullable=true, description="filter on the date of birth", default="")
     * @QueryParam(name="box", strict=true, nullable=true, description="The box to update")
     * @throws NonUniqueResultException
     */
    public function cgetAction(Lot $lot, ParamFetcher $paramFetcher)
    {
        try {
            $box = null;
            if (false === is_null($paramFetcher->get('box'))) {
                $box = $this->boxService->getBox($lot, $paramFetcher->get('box'));
            }
        } catch (BoxNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        }
        return $this->fileService->getFilteredFilesToPack($lot, $paramFetcher->get('firstname'),
            $paramFetcher->get('commonName'), $paramFetcher->get('dob'), $box);
    }

    /**
     * Count the number of Files in the given Lot according to filters
     * @param Lot $lot
     * @param ParamFetcher $paramFetcher
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @Rest\Get("/lots/{lot}/files/count")
     * @SWG\Response(response=200,
     *     description="Return the number of file of the given lot according to filters",
     *     @SWG\Schema(type="integer"))
     * )
     * @SWG\Response(response=404,
     *     description="The lot does not exist"
     * )
     * @QueryParam(name="classified", requirements="(true|false)", strict=true, nullable=true, description="The status of the files")
     * @QueryParam(name="category", requirements="\w{1,2}", strict=true, nullable=true, description="The category of the files")
     */
    public function countAction(Lot $lot, ParamFetcher $paramFetcher)
    {
        $view = $this->view($this->fileService->getNbFiles($lot, "true" === $paramFetcher->get('classified'),
            $paramFetcher->get('category')));

        return $this->handleView($view);
    }

    /**
     * Download the shelves Export: Each file of each shelf of the given Lot
     * @Rest\Get("/lots/{lot}/files/shelves")
     * @SWG\Response(response=200,
     *     description="CSV file",
     * )
     * @SWG\Response(response=404,
     *     description="The lot does not exist"
     * )
     * @param Lot $lot
     * @param ParamFetcher $paramFetcher
     * @return Response
     * @throws \Exception
     */
    public function shelvesAction(Lot $lot, ParamFetcher $paramFetcher)
    {
        $content = $this->fileService->exportShelf($lot);
        $response = new Response($content);
        $date = new \DateTime();
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            sprintf(self::EXPORT_SHELF_TITLE, $date->format('Ymd'))
        );

        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
