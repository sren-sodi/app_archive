<?php

namespace App\Repository;

use App\Entity\File;
use App\Entity\Lot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends ServiceEntityRepository
{

    const FILTER_LIMIT = 20;

    /**
     * FileRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    /**
     * @param File $file
     */
    public function persist(File $file)
    {
        $this->massPersist([$file]);
    }

    /**
     * @param array $files
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function massPersist(array $files)
    {
        foreach ($files as $file) {
            $this->getEntityManager()->persist($file);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * @param Lot $lot
     * @param bool|null $classified
     * @param string|null $categoryCode
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNbFiles(Lot $lot, ?bool $classified = null, ?string $categoryCode = null): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(f.id)')
            ->from($this->getEntityName(), 'f')
            ->join('f.page', 'p')
            ->join('p.lot', 'l')
            ->where($qb->expr()->eq('l', ':lot'));
        if (isset($classified)) {
            if (true === $classified) {
                $qb->andWhere($qb->expr()->isNotNull('f.category'));
            } else {
                $qb->andWhere($qb->expr()->isNull('f.category'));
            }
        }
        if (isset($categoryCode) && false === empty($categoryCode)) {
            $qb->join('f.category', 'c');
            $qb->andWhere($qb->expr()->eq('c.code', ':code'));
            $qb->setParameter('code', $categoryCode);
        }
        $qb->setParameter('lot', $lot);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get Next File to Pack for the given Lot
     * @param Lot $lot
     * @return File
     * @throws NonUniqueResultException
     */
    public function getNextFileToPack(Lot $lot): ?File
    {
        $qb = $this->getOrderedFileToPack($lot);
        $qb->andWhere($qb->expr()->isNull('f.box'));
        $qb->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Lot $lot
     * @return QueryBuilder
     */
    private function getOrderedFileToPack(Lot $lot): QueryBuilder
    {
        $qb = $this->getFileToPack($lot);
        $qb->addOrderBy("f.commonName", "ASC");
        $qb->addOrderBy("f.firstname", "ASC");

        return $qb;
    }

    /**
     * @param Lot $lot
     * @return QueryBuilder
     */
    private function getFileToPack(Lot $lot): QueryBuilder
    {
        $qb = $this->createQueryBuilder('f');
        $qb->join('f.page', 'p')
            ->join('p.lot', 'l')
            ->join('f.category', 'c')
            ->where($qb->expr()->eq('l', ':lot'))
            ->andWhere($qb->expr()->eq('c.code', ':category'))
//            ->andWhere($qb->expr()->isNull('f.box'))
            ->setParameters(
                [
                    'category' => 'P',
                    'lot' => $lot,
                ]
            );

        return $qb;
    }

    /**
     * @param Lot $lot
     * @return array
     */
    public function getFilesToPack(Lot $lot): array
    {
        $qb = $qb = $this->getOrderedFileToPack($lot);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param File $from
     * @param string|null $firstname
     * @param string|null $commonName
     * @param string|null $dob
     * @return array
     */
    public function getFilteredFilesToPack(File $from, ?string $firstname, ?string $commonName, ?string $dob): array
    {
        $qb = $this->getOrderedFileToPack($from->getPage()->getLot());
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->andX(
                // between commonNames
                    $qb->expr()->gt('f.commonName', ':from_commonName'),
                    ),
                $qb->expr()->andX(
                //case of equals commonName
                    $qb->expr()->eq('f.commonName', ':from_commonName'),
                    $qb->expr()->gt('f.firstname', ':from_firstname'),
                    ),
                $qb->expr()->eq('f', ':from')
                ,
                ))
            ->setParameter('from', $from)
            ->setParameter('from_commonName', $from->getCommonName())
            ->setParameter('from_firstname', $from->getFirstname());
        if (false == empty($firstname)) {
            $qb->andWhere($qb->expr()->like('LOWER(f.firstname)', ':firstname'));
            $qb->setParameter('firstname', '%' . strtolower($firstname) . '%');
        }
        if (false == empty($commonName)) {
            $qb->andWhere($qb->expr()->like('LOWER(f.commonName)', ':commonName'));
            $qb->setParameter('commonName', '%' . strtolower($commonName) . '%');
        }
        if (false == empty($dob)) {
            //force format to be able to use LIKE
            $qb->andWhere($qb->expr()->like("DATE_FORMAT(f.dob,'%d/%m/%Y')", ':dob'));
            $qb->setParameter('dob', '%' . $dob . '%');
        }
        $qb->setMaxResults(self::FILTER_LIMIT);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Lot $lot
     * @param File $from
     * @param File $to
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function estimateRange(File $from, File $to): int
    {
        $qb = $this->getFilesRangeQB($from, $to);
        $qb->select('count(f)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get a file range to pack
     * @param Lot $lot
     * @param File $from
     * @param File $to
     * @return QueryBuilder
     */
    private function getFilesRangeQB(File $from, File $to): QueryBuilder
    {
        $qb = $this->getFileToPack($from->getPage()->getLot());
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->andX(
                // between commonNames
                    $qb->expr()->gt('f.commonName', ':from_commonName'),
                    $qb->expr()->lt('f.commonName', ':to_commonName'),
                    ),
                $qb->expr()->andX(
                //case of equals commonName
                    $qb->expr()->eq('f.commonName', ':from_commonName'),
                    $qb->expr()->gt('f.firstname', ':from_firstname'),
                    ),
                $qb->expr()->andX(
                //case of equals commonName
                    $qb->expr()->eq('f.commonName', ':to_commonName'),
                    $qb->expr()->lt('f.firstname', ':to_firstname'),
                    )
                ,
                //borders
                $qb->expr()->eq('f', ':from'),
                $qb->expr()->eq('f', ':to'),
                ))
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('from_commonName', $from->getCommonName())
            ->setParameter('to_commonName', $to->getCommonName())
            ->setParameter('from_firstname', $from->getFirstname())
            ->setParameter('to_firstname', $to->getFirstname());

        return $qb;
    }

    /**
     * @param File $from
     * @param File $to
     * @return array
     */
    public function getFileRange(File $from, File $to): array
    {
        $qb = $this->getFilesRangeQB($from, $to);
        $qb->addOrderBy("f.commonName", "ASC");
        $qb->addOrderBy("f.firstname", "ASC");

        return $qb->getQuery()->getResult();
    }

    public function getFistOfEachShelf(Lot $lot): array
    {
        $shelves = $this->getDistinctShelves($lot);

        return array_map(function (int $shelf) use ($lot) {
            return $this->getFirstOfShelf($lot, $shelf);
        }, $shelves);
    }

    public function getDistinctShelves(Lot $lot): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('f.shelf')
            ->distinct()
            ->from($this->getEntityName(), 'f')
            ->join('f.page', 'p')
            ->join('p.lot', 'l')
            ->where($qb->expr()->eq('l', ':lot'))
            ->setParameter('lot', $lot)
            ->orderBy('f.shelf', 'ASC');
        return array_column($qb->getQuery()->getScalarResult(), 'shelf');
    }

    public function getFirstOfShelf(Lot $lot, int $shelf): ?File
    {
        $qb = $this->createQueryBuilder('f');
        $qb->join('f.page', 'p')
            ->join('p.lot', 'l')
            ->where($qb->expr()->eq('l', ':lot'))
            ->andWhere($qb->expr()->eq('f.shelf', ':shelf'))
            ->setParameter('lot', $lot)
            ->setParameter('shelf', $shelf)
            ->setMaxResults(1);
        $qb->addOrderBy("f.commonName", "ASC");
        $qb->addOrderBy("f.firstname", "ASC");
        return $qb->getQuery()->getOneOrNullResult();
    }
//
//    public function unboxFilesFrom(Box $box){
//        $qb = $this->getEntityManager()->createQueryBuilder();
//        $qb->update($this->getEntityName(),'f')
//            ->set('f.box',':no_box')
//            ->where($qb->expr()->eq('f.box',':box'))
//            ->setParameter('box',$box)
//            ->setParameter('no_box',null);
//        $qb->getQuery()->execute();
//    }
}
