<?php

namespace App\Repository;

use App\Entity\Lot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LotRepository extends ServiceEntityRepository
{
    /**
     * LotRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lot::class);
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function findAll(?int $limit = null, ?int $offset = null): array
    {
        $qb = $this->createQueryBuilder('l');
        if (isset($offset)) {
            $qb->setFirstResult($offset);
        }
        if (isset($limit)) {
            $qb->setMaxResults($limit);
        }

        $qb->orderBy('l.number', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Lot|null
     * @throws NonUniqueResultException
     */
    public function getHighestLot(): ?Lot
    {
        $qb = $this->createQueryBuilder('l');
        $qb->setMaxResults(1);
        $qb->orderBy('l.number', 'DESC');
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countLots(): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(l)')
            ->from(Lot::class, 'l');

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Lot $lot
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Lot $lot)
    {
        $this->getEntityManager()->persist($lot);
        $this->getEntityManager()->flush();
    }
}
