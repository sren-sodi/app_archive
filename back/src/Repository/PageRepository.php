<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{

    /**
     * PageRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    /**
     * @return Criteria
     */
    public static function getValidatedPageCriteria(): Criteria
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->neq('validated_at', null));

        return $criteria;
    }

    /**
     * @return Criteria
     */
    public static function getFirstNotValidated(): Criteria
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->isNull('validated_at'));
        $criteria->orderBy(['number' => 'ASC']);
        $criteria->setMaxResults(1);

        return $criteria;
    }

    /**
     * @return Criteria
     */
    public static function getLastValidated(): Criteria
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->neq('validated_at', null));
        $criteria->orderBy(
            [
                'validated_at' => 'DESC',
                'number' => 'DESC',
            ]
        );
        $criteria->setMaxResults(1);

        return $criteria;
    }

    /**
     * @param Page $page
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Page $page)
    {
        $this->getEntityManager()->persist($page);
        $this->getEntityManager()->flush();
    }
}
