<?php

namespace App\Repository;

use App\Entity\Box;
use App\Entity\Lot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Box|null find($id, $lockMode = null, $lockVersion = null)
 * @method Box|null findOneBy(array $criteria, array $orderBy = null)
 * @method Box[]    findAll()
 * @method Box[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoxRepository extends ServiceEntityRepository
{

    /**
     * BoxRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Box::class);
    }

    /**
     * Get the list of Boxes based on a Lot of files
     * @param Lot $lot
     * @return array
     */
    public function getBoxes(Lot $lot): array
    {
        $qb = $this->getBoxQuery($lot);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Lot $lot
     * @return QueryBuilder
     */
    private function getBoxQuery(Lot $lot): QueryBuilder
    {
        $qb = $this->createQueryBuilder('b');
        $qb->join('b.files', 'f')
            ->join('f.page', 'p')
            ->join('p.lot', 'l')
            ->where($qb->expr()->eq('l', ':lot'))
            ->setParameter('lot', $lot)
            ->orderBy('b.number', 'DESC');

        return $qb;
    }

    /**
     * @param Lot $lot
     * @return Box|null
     * @throws NonUniqueResultException
     */
    public function getLastBox(Lot $lot): ?Box
    {
        $qb = $this->getBoxQuery($lot);
        $qb->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Box $box
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Box $box)
    {
        $this->getEntityManager()->persist($box);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Box $box
     * @return array
     */
    public function getNextBoxes(Box $box): array
    {
        $qb = $this->createQueryBuilder('b');
        $qb->join('b.lot', 'l')
            ->where($qb->expr()->gt('b.number', ':number'))
            ->andWhere($qb->expr()->eq('l', ':lot'))
            ->setParameter('number', $box->getNumber())
            ->setParameter('lot', $box->getLot());
        return $qb->getQuery()->getResult();
    }

    /**
     * @param Box $box
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Box $box)
    {
        $this->getEntityManager()->remove($box);
        $this->getEntityManager()->flush();
    }
}
