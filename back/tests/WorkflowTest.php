<?php


namespace App\Tests;


use App\Entity\Lot;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Workflow\Workflow;

class WorkflowTest extends KernelTestCase
{

    /**
     * @var ObjectManager
     */
    private static $manager;


    /**
     * @var Workflow
     */
    private static $workflow;

    public static function setUpBeforeClass(): void
    {
        parent::bootKernel();
        self::$manager = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        self::$workflow = self::$kernel->getContainer()->get('state_machine.archivage');
    }

    public function testStartingState()
    {
        $lot = new Lot();
        $lot->setNumber(1);
        $lot->setDobFrom(new DateTime('-10 year'));
        $lot->setDobTo(new DateTime('-5 year'));
        $lot->setWithAffiliation(false);
        $lot->setWithNir(false);

        self::$manager->persist($lot);
        self::$manager->flush();

        $this->assertEquals(current(self::$workflow->getDefinition()->getInitialPlaces()), $lot->getState());
    }

    public function testGuardSubmitFail()
    {
        $lot = DataGenerator::generateLot();

        $this->assertFalse(self::$workflow->can($lot, 'submit'));
    }

    public function testGuardSubmitSuccess()
    {
        /**
         * @var Lot $lot
         */
        $lot = DataGenerator::generateLot();

        $lot->setLastShelfExportedAt(new DateTime());
        foreach ($lot->getPages() as $page) {
            $page->setValidatedAt(new DateTime());
        }

        $this->assertTrue(self::$workflow->can($lot, 'submit'));
    }

    public function testOnSubmitEventSuccess()
    {
        /**
         * @var Lot $lot
         */
        $lot = DataGenerator::generateLot();
        $lot->setLastShelfExportedAt(new DateTime());
        foreach ($lot->getPages() as $page) {
            $page->setValidatedAt(new DateTime());
        }

        self::$workflow->apply($lot, 'submit');

        $this->assertNotNull($lot->getSubmitedAt());
    }
}