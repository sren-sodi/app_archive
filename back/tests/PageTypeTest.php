<?php


namespace App\Tests;


use App\DataTransformer\FileCategoryTransformer;
use App\DataTransformer\FileCategoryViewTransformer;
use App\Entity\File;
use App\Entity\FileCategory;
use App\Entity\Page;
use App\Form\FileCategoryType;
use App\Form\PageType;
use App\Kernel;
use App\Service\Entity\FileCategoryService;
use DateTime;
use Symfony\Bridge\PhpUnit\SetUpTearDownTrait;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\ContainerConstraintValidatorFactory;
use Symfony\Component\Validator\Validation;

class PageTypeTest extends TypeTestCase
{
    use SetUpTearDownTrait;

    private $fileTypeService;
    private $kernel;

    public function testValidationSuccess()
    {
        $page = self::getTargetPage();
        $page->getLot()->setLastShelfExportedAt(new DateTime());
        $first = $page->getFiles()->first();
        $page->getFiles()->clear();
        $page->addFile($first);
        $data = [
            'files' => [
                [
                    'category' => 'C',
                ],
            ],
        ];

        $form = $this->factory->create(PageType::class, $page);
        $form->submit($data);
        foreach ($form->getErrors(true) as $error) {
            dump($error->getMessage());
        }        //dump($form->getData());
        $this->assertTrue($form->isValid());
    }

    private static function getTargetPage(): Page
    {
        $file = DataGenerator::getTargetFile();

        return $file->getPage();
    }

    public function testMissingCategoryFail()
    {
        $page = self::getTargetPage();
        $file = new File();
        $file->setNumber("2");
        $file->setShelf(2);
        $file->setDob(new DateTime());
        $file->setFirstname("dd");
        $file->setLastname("tdd");
        $page->addFile($file);
        $data = [
            'files' => [
                [
                    'category' => 'C',
                ],
            ],
        ];

        $form = $this->factory->create(PageType::class, $page);
        $form->submit($data);
        /*foreach ($form->getErrors(true) as $error) {
            dump($error->getOrigin()->getName().' => '.$error->getMessage());
        }*/
        $this->assertFalse($form->isValid());
    }

    protected function getExtensions()
    {
        $validatorBuilder = Validation::createValidatorBuilder();
        $validatorBuilder->setConstraintValidatorFactory(
            new ContainerConstraintValidatorFactory($this->kernel->getContainer())
        );
        $validator = $validatorBuilder->enableAnnotationMapping()
            ->getValidator();

        return [
            new ValidatorExtension($validator),
            new PreloadedExtension([
                new FileCategoryType(new FileCategoryTransformer($this->fileTypeService),
                    $this->kernel->getContainer()->get(FileCategoryViewTransformer::class))
            ], []),
        ];
    }

    protected function setUp(): void
    {
        $category = new FileCategory();
        $category->setCode('C');

        $this->kernel = new Kernel('test', true);
        $this->kernel->boot();

        // mock any dependencies
        $this->fileTypeService = $this->createMock(FileCategoryService::class);
        $this->fileTypeService->expects($this->any())->method('findByCode')->willReturn($category);
        parent::setUp();
    }
}