<?php


namespace App\Tests;


use App\Entity\Lot;
use App\Form\LotType;
use App\Kernel;
use App\Service\Transverse\PensionService;
use Symfony\Bridge\PhpUnit\SetUpTearDownTrait;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\PropertyAccess\Exception\InvalidArgumentException;
use Symfony\Component\Validator\ContainerConstraintValidatorFactory;
use Symfony\Component\Validator\Validation;

class LotTypeTest extends TypeTestCase
{

    use SetUpTearDownTrait;


    private $pensionService;

    /**
     * @var Kernel
     */
    static $kernel;


    /**
     * Test valid lot submission
     */
    public function testCreateLotSuccess()
    {
        $lot = new Lot();
        $form = $this->factory->create(LotType::class, $lot);

        $values = [
            'number' => 0,
            'withNir' => 0,
            'withAffiliation' => 1,
            'dobTo' => "1940-09-07",
            'dobFrom' => "1920-09-06",
        ];
        /**
         * @var FormInterface $form
         */
        $form->submit($values);
        foreach ($form->getErrors(true) as $error) {
            dump($error->getOrigin()->getName().' '.$error->getMessage());
        }
        $this->assertTrue($form->isValid());
    }

    /**
     * Test invalid lot submission
     */
    public function testMissingNumberLotFailed()
    {
        $lot = new Lot();
        $form = $this->factory->create(LotType::class, $lot);

        $values = [
            'withNir' => 0,
            'withAffiliation' => "0",
            'dobTo' => "1940-09-07",
            'dobFrom' => "1920-09-06",
        ];
        /**
         * @var FormInterface $form
         */
        $form->submit($values);
        //dump($form->getErrors());
        $this->assertFalse($form->isValid());
    }
    
    /**
     * Test invalid lot submission
     */
    public function testMissingDobToLotFailed()
    {
        $lot = new Lot();
        $form = $this->factory->create(LotType::class, $lot);

        $values = [
            'number' => 1,
            'withNir' => 0,
            'withAffiliation' => "0",
            'dobFrom' => "1920-09-06",
        ];
        /**
         * @var FormInterface $form
         */
        $form->submit($values);
        //dump($form->getErrors());
        $this->assertFalse($form->isValid());
    }

    /**
     * Test invalid lot submission
     */
    public function testMissingDobFromLotFailed()
    {
        $lot = new Lot();
        $form = $this->factory->create(LotType::class, $lot);

        $values = [
            'number' => 1,
            'withNir' => 0,
            'withAffiliation' => 0,
            'dobFrom' => "1920-09-06",
        ];

        /**
         * @var FormInterface $form
         */
        $form->submit($values);
        //dump($form->getErrors());
        $this->assertFalse($form->isValid());
    }

    /**
     * Test invalid lot submission
     */
    public function testBadFormatDobFromLotFailed()
    {
        $lot = new Lot();
        $form = $this->factory->create(LotType::class, $lot);

        $values = [
            'number' => 1,
            'withNir' => 0,
            'withAffiliation' => 0,
            'dobTo' => "19-09-07",
            'dobFrom' => "1920-09-06",
        ];

        /**
         * @var FormInterface $form
         */
        $form->submit($values);
        //dump($form->getData());
        $this->assertFalse($form->isValid());
    }

    /**
     * Test invalid lot submission
     */
    public function testBadFormatNumberLotFailed()
    {
        $lot = new Lot();
        $form = $this->factory->create(LotType::class, $lot);

        $values = [
            'number' => "AA1AA",
            'withNir' => 0,
            'withAffiliation' => 0,
            'dobTo' => "2019-09-07",
            'dobFrom' => "1920-09-06",
        ];

        $this->expectException(InvalidArgumentException::class);
        /**
         * @var FormInterface $form
         */
        $form->submit($values);
        //dump($form->getErrors());
    }

    /**
     * Test invalid lot submission
     */
    public function testDobFromAfterToLotFailed()
    {
        $lot = new Lot();
        $form = $this->factory->create(LotType::class, $lot);

        $values = [
            'number' => 1,
            'withNir' => 0,
            'withAffiliation' => 0,
            'dobTo' => "2018-09-05",
            'dobFrom' => "2019-09-06",
        ];

        /**
         * @var FormInterface $form
         */
        $form->submit($values);
        //dump($form->getData());
        $this->assertFalse($form->isValid());
    }

    protected function setUp(): void
    {
        self::$kernel = new Kernel('test', true);
        self::$kernel->boot();

        // mock any dependencies
        $this->pensionService = $this->createMock(PensionService::class);
        $this->pensionService->expects($this->any())->method('getFilesForLot')->willReturn([]);

        parent::setUp();
    }

    protected function getExtensions()
    {
        $validatorBuilder = Validation::createValidatorBuilder();
        $validatorBuilder->setConstraintValidatorFactory(
            new ContainerConstraintValidatorFactory(self::$kernel->getContainer())
        );
        $validator = $validatorBuilder->enableAnnotationMapping()
            ->getValidator();

        return [
            new ValidatorExtension($validator),
            new PreloadedExtension([new LotType($this->pensionService)], []),
        ];
    }
}