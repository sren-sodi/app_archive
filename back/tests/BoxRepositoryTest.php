<?php


namespace App\Tests;


use App\Entity\Box;
use App\Entity\File;
use App\Entity\FileCategory;
use App\Repository\BoxRepository;
use App\Repository\FileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class BoxRepositoryTest
 * @package App\Tests
 */
class BoxRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var BoxRepository
     */
    private $boxRepository;

    /**
     * @var array
     */
    private $category;

    /**
     * @var FileRepository
     */
    private $fileRepository;

    public function setUp(): void
    {
        $this->bootKernel();
        $this->manager = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        $this->boxRepository = $this->manager->getRepository(Box::class);
        $this->fileRepository = $this->manager->getRepository(File::class);

        $this->category = array_map(
            function (FileCategory $category) {
                $this->manager->persist($category);
                $this->manager->flush();

                return $category;
            },
            DataGenerator::buildCategory()
        );
    }

    public function testgetEmptyBoxes()
    {
        $lot = DataGenerator::generateLot();
        $this->manager->persist($lot);
        $this->manager->flush();

        $boxes = $this->boxRepository->getBoxes($lot);

        $this->assertEquals(0, count($boxes));
    }

    public function testGetNotEmptyBoxes()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);

        $this->manager->persist($lot);
        for ($i = 0; $i < 3; $i++) {
            $box = DataGenerator::generateBox($lot, $i + 1);
            $this->manager->persist($box);
        }
        $this->manager->flush();

        $boxes = $this->boxRepository->getBoxes($lot);

        $this->assertEquals(3, count($boxes));
    }

    public function testSecondGetNextBoxes()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);

        $this->manager->persist($lot);
        $this->manager->flush();

        for ($i = 0; $i < 5; $i++) {
            $box = DataGenerator::generateBox($lot, $i + 1);
            $this->manager->persist($box);
        }
        $this->manager->flush();
        $from = $lot->getBoxes()->get(1);
        $boxes = $this->boxRepository->getNextBoxes($from);
        $filters = array_filter(array_map(function (Box $box) use ($from) {
            return $box->getNumber() > $from->getNumber() && $box->getLot()->getId() === $from->getLot()->getId();
        }, $boxes));
        $this->assertEquals(3, count($filters));
    }

    public function testLastGetNextBoxes()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);

        $this->manager->persist($lot);
        $this->manager->flush();

        for ($i = 0; $i < 5; $i++) {
            $box = DataGenerator::generateBox($lot, $i + 1);
            $this->manager->persist($box);
        }
        $this->manager->flush();
        $from = $lot->getBoxes()->last();
        $boxes = $this->boxRepository->getNextBoxes($from);
        $filters = array_filter(array_map(function (Box $box) use ($from) {
            return $box->getNumber() > $from->getNumber() && $box->getLot()->getId() === $from->getLot()->getId();
        }, $boxes));
        $this->assertEquals(0, count($filters));
    }
}