<?php


namespace App\Tests;


use App\DataTransformer\FileCategoryTransformer;
use App\DataTransformer\FileCategoryViewTransformer;
use App\Entity\FileCategory;
use App\Exception\FileCategoryNotFound;
use App\Form\FileCategoryType;
use App\Form\FileType;
use App\Kernel;
use App\Service\Entity\FileCategoryService;
use Symfony\Bridge\PhpUnit\SetUpTearDownTrait;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;

class FileTypeTest extends TypeTestCase
{
    use SetUpTearDownTrait;

    private $fileTypeService;
    private $fileCategoryTransformer;
    private $fileService;

    /**
     * @var Kernel
     */
    private $kernel;

    public
    function testSetCategorySuccess()
    {
        $data = [
            'category' => "C",
        ];

        $file = DataGenerator::getTargetFile();

        $form = $this->factory->create(FileType::class, $file);

        /**
         * @var FormInterface $form
         */
        $form->submit($data);
        /*foreach ($form->getErrors(true) as $error) {
            dump($error->getOrigin()->getName()." = > ".$error->getMessage());
        }*/
        $this->assertTrue($form->isValid());
    }

    public function testSetWrongCategoryFail()
    {
        $data = [
            'category' => "AA",
        ];

        $file = DataGenerator::getTargetFile();

        $form = $this->factory->create(FileType::class, $file);

        /**
         * @var FormInterface $form
         */
        $form->submit($data);
        /*foreach ($form->getErrors(true) as $error) {
            dump($error->getOrigin()->getName()." = > ".$error->getMessage());
        }*/
        $this->assertFalse($form->isValid());
    }

    public function testMissingNumberSetCategoryFail()
    {
        $data = [
            'number' => '1',
        ];

        $file = DataGenerator::getTargetFile();

        $form = $this->factory->create(FileType::class, $file);

        /**
         * @var FormInterface $form
         */
        $form->submit($data);
        //dump($form->getData());
        $this->assertFalse($form->isValid());
    }

    protected function setUp(): void
    {
        $this->kernel = new Kernel('test', true);
        $this->kernel->boot();

        // mock any dependencies
        $this->fileTypeService = $this->createMock(FileCategoryService::class);
        $this->fileTypeService->expects($this->any())->method('findByCode')->willReturnCallback(
            function (string $code) {
                return self::getCategory($code);
            }
        );

        parent::setUp();
    }

    public static function getCategory(string $code): FileCategory
    {
        $cats = DataGenerator::buildCategory();
        $category = isset($cats[$code]) ? $cats[$code] : null;
        if (false === $category instanceof FileCategory) {
            throw new FileCategoryNotFound();
        }

        return $category;
    }

    protected
    function getExtensions()
    {
        // or if you also need to read constraints from annotations
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        return [
            new ValidatorExtension($validator),
            new PreloadedExtension([new FileCategoryType(new FileCategoryTransformer($this->fileTypeService), $this->kernel->getContainer()->get(FileCategoryViewTransformer::class))], []),
        ];
    }
}