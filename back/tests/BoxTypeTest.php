<?php


namespace App\Tests;


use App\DataTransformer\FileTransformer;
use App\DataTransformer\FileViewTransformer;
use App\Entity\Box;
use App\Entity\File;
use App\Entity\FileCategory;
use App\Form\BoxType;
use App\Form\SelectFileType;
use App\Kernel;
use App\Service\Entity\BoxService;
use App\Service\Entity\FileService;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\ContainerConstraintValidatorFactory;
use Symfony\Component\Validator\Validation;

class BoxTypeTest extends TypeTestCase
{
    /**
     * @var Kernel
     */
    static $kernel;

    /**
     * @var FileService
     */
    static $fileService;
    /**
     * @var BoxService
     */
    static $boxService;

    public function testSelectRangeType()
    {
        $data = [
            'lastFile' => '55',
        ];
        $category = new FileCategory();
        $category->setCode('P');
        $box = DataGenerator::generateBox(DataGenerator::generatePensionLot($category), 0);
        $form = $this->factory->create(BoxType::class, $box);
        $form->submit($data);
        $this->assertTrue($box->getLastFile() instanceof File);
    }

    public function testSelectRangeNumber()
    {
        $category = new FileCategory();
        $category->setCode('P');
        $box = DataGenerator::generateBox(DataGenerator::generatePensionLot($category), 0);
        $files = $box->getFiles()->toArray();
        $data = [
            'lastFile' => $files[2]->getNumber(),
        ];
        $box->getFiles()->clear();
        $box->addFile($files[0]);
        $form = $this->factory->create(BoxType::class, $box);
        $form->submit($data);
//        foreach ($form->getErrors() as $error) {
//            dump($error->getMessage());
//        }
        $this->assertEquals(3, $box->getFiles()->count());
    }

    protected function getExtensions()
    {
        $validatorBuilder = Validation::createValidatorBuilder();
        $validatorBuilder->setConstraintValidatorFactory(
            new ContainerConstraintValidatorFactory(self::$kernel->getContainer())
        );
        $validator = $validatorBuilder->enableAnnotationMapping()
            ->getValidator();

        return [
            new ValidatorExtension($validator),
            new PreloadedExtension([
                new SelectFileType(new FileTransformer(self::$fileService), new FileViewTransformer(self::$fileService)),
                new BoxType(self::$boxService),
            ], []),

        ];
    }

    protected function setUp(): void
    {
        $files = DataGenerator::generateFiles(1);
        $file = current($files);
        self::$kernel = new Kernel('test', true);
        self::$kernel->boot();

        // mock any dependencies
        self::$fileService = $this->createMock(FileService::class);
        self::$fileService->expects($this->any())->method('findByNumber')->willReturn($file);
        self::$boxService = $this->createMock(BoxService::class);
        self::$boxService->expects($this->any())->method('setFiles')->willReturnCallback(function (Box $box, File $to) {
            $files = DataGenerator::generateFiles(1);
            $file = current($files);
            $box->addFile($file);
            $box->addFile($to);

            return $box;
        });
        parent::setUp();
    }
}