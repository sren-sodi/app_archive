<?php


namespace App\Tests;


use App\Entity\Lot;
use App\Service\Transverse\PensionService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PensionServiceTest extends KernelTestCase
{
    public function testInterval()
    {
        $lot = new Lot();
        $lot->setDobFrom(new DateTime('1950-01-01'));
        $lot->setDobTo(new DateTime('1951-01-01'));
        $lot->setWithNir(false);
        $lot->setWithAffiliation(false);

        $queries = PensionService::buildQueriesParam($lot);
        $this->assertEquals(25, count($queries));
    }
}