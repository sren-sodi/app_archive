<?php


namespace App\Tests;


use App\Entity\File;
use App\Entity\FileCategory;
use App\Entity\Lot;
use App\Entity\Page;
use App\Kernel;
use App\Repository\FileRepository;
use App\Repository\LotRepository;
use App\Service\Entity\PageService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\PhpUnit\SetUpTearDownTrait;
use Symfony\Component\HttpKernel\KernelInterface;

class LotStatTest extends TestCase
{

    use SetUpTearDownTrait;

    /**
     * @var KernelInterface
     */
    private $kernel;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var LotRepository
     */
    private $lotRepository;
    private $fileCategoryRepo;

    /**
     * @var PageService
     */
    private $pageService;


    /**
     * @var FileRepository
     */
    private $fileRepository;

    public function setUp(): void
    {
        $this->kernel = new Kernel('test', true);
        $this->kernel->boot();
        $this->manager = $this->kernel->getContainer()->get('doctrine.orm.default_entity_manager');

        $this->lotRepository = $this->manager->getRepository(Lot::class);
        $this->fileCategoryRepo = $this->manager->getRepository(FileCategory::class);
        $this->pageService = $this->manager->getRepository(Page::class);
        $this->fileRepository = $this->manager->getRepository(File::class);

    }

    public function testAffectCategory()
    {
        $targetedLotNumber = 1;
        $categoryCode = 'C';

        $lot = $this->getLot();
        /**
         * @var Page $page
         */
        $page = $lot->getPages()->first();
        $category = $this->getCategory($categoryCode);
        PageService::affectCategory($page, $category);
        $test = $page->getFiles()->forAll(
            function ($index, File $file) use ($category) {
                return $file->getCategory() === $category;
            }
        );
        self::assertTrue($test);
    }

    private function getLot(): Lot
    {
        return DataGenerator::generateLot();
    }

    private function getCategory(string $categoryCode): FileCategory
    {
        /**
         * @var FileCategory $category
         */
        $category = $this->fileCategoryRepo->findOneBy(
            [
                'code' => $categoryCode,
            ]
        );

        return $category;
    }

    /**
     * @return array
     */
    public function getPourcentagesProvider(): array
    {
        return [
            [0],
            [25],
            [50],
            [75],
            [100],
        ];
    }

    /**
     * @dataProvider getPourcentagesProvider
     * @param int $pourcentage
     * @throws Exception
     */
    public function testPourcentageValidated(int $pourcentage)
    {
        $targetedLotNumber = 1;
        $lot = $this->getLot();
        $nbPages = $lot->getNbPages();
        $pagesToValidate = $nbPages * ($pourcentage / 100);
        for ($i = 0; $i < $pagesToValidate; $i++) {
            $page = $lot->getPages()->get($i);
            /**
             * @var Page $page
             */
            $page->setValidatedAt(new DateTime());
        }
        $this->manager->persist($lot);
        $this->manager->flush();
        $this->assertEquals($pourcentage, 100 * $lot->getNbValidatedPages() / $nbPages);
    }

    /**
     * @throws Exception
     */
    public function testLastValidated()
    {
        $lot = $this->getLot();
        $nbPages = $lot->getNbPages();
        $page = null;
        for ($i = 0; $i < 10; $i++) {
            $page = $lot->getPages()->get($i);
            /**
             * @var Page $page
             */
            $page->setValidatedAt(new DateTime('-1 day'));
        }
        $page = $lot->getPages()->first();
        $page->setValidatedAt(new DateTime());
        $this->manager->persist($lot);
        $this->manager->flush();
        $this->assertEquals($page, $lot->getLastValidatedPage());
    }

    /**
     * @throws Exception
     */
    public function testFirstNotValidated()
    {
        $lot = $this->getLot();
        $nbPages = $lot->getNbPages();
        $page = null;
        for ($i = 0; $i < $nbPages; $i++) {
            $page = $lot->getPages()->get($i);
            /**
             * @var Page $page
             */
            $page->setValidatedAt(new DateTime());
        }
        $page1 = $lot->getPages()->get(2);
        $page1->setValidatedAt(null);
        $page2 = $lot->getPages()->get(6);
        $page2->setValidatedAt(null);
        $this->manager->persist($lot);
        $this->manager->flush();
        $this->assertEquals($page1, $lot->getFirstNotValidatedPage());
    }

    /**
     * @dataProvider getPourcentagesProvider
     * @param int $pourcentage
     * @throws Exception
     */
    public function testNumberClassifiedFile(int $pourcentage)
    {
        $lot = $this->getLot();
        $nbPages = $lot->getNbPages();
        $pagesToValidate = $nbPages * ($pourcentage / 100);
        $category = $this->getCategory('C');
        for ($i = 0; $i < $pagesToValidate; $i++) {
            $page = $lot->getPages()->get($i);
            if ($i < $pagesToValidate) {
                PageService::affectCategory($page, $category);
            } else {
                PageService::affectCategory($page, null);
            }
        }
        $this->manager->persist($lot);
        $this->manager->flush();
        $this->assertEquals($pourcentage, 100 * $this->fileRepository->getNbFiles($lot, true) / $lot->getNbFiles());
    }

    /**
     * @dataProvider getPourcentagesProvider
     * @param int $pourcentage
     * @throws Exception
     */
    public function testNumberClassifiedCategoryFile(int $pourcentage)
    {
        $lot = $this->getLot();
        $nbPages = $lot->getNbPages();
        $pagesToValidate = $nbPages * ($pourcentage / 100);
        $category = $this->getCategory('C');
        $otherCategory = $this->getCategory('NT');
        for ($i = 0; $i < $nbPages; $i++) {
            $page = $lot->getPages()->get($i);

            if ($i < $pagesToValidate) {
                PageService::affectCategory($page, $category);
            } else {
                PageService::affectCategory($page, $otherCategory);
            }
        }
        $this->manager->persist($lot);
        $this->manager->flush();
        $this->assertEquals($pourcentage, 100 * $this->fileRepository->getNbFiles($lot, true, 'C') / $lot->getNbFiles());
    }
}