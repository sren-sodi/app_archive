<?php


namespace App\Tests;


use App\Entity\Box;
use App\Entity\FileCategory;
use App\Exception\FormErrorException;
use App\Service\Entity\BoxService;
use App\Service\Entity\FileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BoxServiceTest extends KernelTestCase
{

    static $boxService;

    /**
     * @var EntityManagerInterface
     */
    static $manager;
    static $category;
    static $fileService;

    public function setUp(): void
    {
        $this->bootKernel();
        self::$boxService = self::$kernel->getContainer()->get(BoxService::class);
        self::$fileService = self::$kernel->getContainer()->get(FileService::class);

        self::$manager = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        self::$category = array_map(
            function (FileCategory $category) {
                self::$manager->persist($category);

                return $category;
            },
            DataGenerator::buildCategory()
        );
        self::$manager->flush();

    }

    public function testGenerateBoxNumber()
    {
        $lot = DataGenerator::generatePensionLot(self::$category['P']);
        self::$manager->persist($lot);
        self::$manager->flush();
        $box = self::$boxService->initiateFromLot($lot);
        $this->assertEquals(1, $box->getNumber());
    }

    public function testGenerateBoxFirstFileNotNull()
    {
        $lot = DataGenerator::generatePensionLot(self::$category['P']);
        self::$manager->persist($lot);
        self::$manager->flush();
        $box = self::$boxService->initiateFromLot($lot);
        $this->assertNotNull($box->getFiles()->first());
    }

    public function testGenerateBoxFirstFile()
    {
        $lot = DataGenerator::generatePensionLot(self::$category['P']);
        self::$manager->persist($lot);
        self::$manager->flush();
        $box1 = self::$boxService->initiateFromLot($lot);
        self::$manager->persist($box1);
        self::$manager->flush();
        $box2 = self::$boxService->initiateFromLot($lot);
        $this->assertNotEquals($box2->getFiles()->first(), $box1->getFiles()->first());
    }

    public function testSetFileRange()
    {
        $lot = DataGenerator::generatePensionLot(self::$category['P']);
        self::$manager->persist($lot);
        self::$manager->flush();
        $files = self::$fileService->getFilesToPack($lot);
        array_shift($files); //remove first because it is used to initiateFromLot
        $box1 = self::$boxService->initiateFromLot($lot);
        $indice = array_rand($files);
        self::$boxService->setFiles($box1, $files[$indice]);
        // +2 because it contains border files
        $this->assertEquals($indice + 2, $box1->getFiles()->count());
    }

    public function testEditAndRemoveNext()
    {
        $lot = DataGenerator::generatePensionLot(self::$category['P']);

        self::$manager->persist($lot);
        self::$manager->flush();
        $boxes = [];
        for ($i = 0; $i < 3; $i++) {
            $box = self::$boxService->initiateFromLot($lot);
            self::$boxService->setFiles($box, $box->getFirstFile());
            self::$manager->persist($box);
            self::$manager->flush();
            $boxes[$i] = $box;
        }

        try {
            $boxes[1] = self::$boxService->editAndRemoveNextBoxes($boxes[0], [
                'lastFile' => $boxes[1]->getLastFile()->getNumber(),
            ]);
        } catch (FormErrorException $exception) {
            foreach ($exception->getFormError() as $item) {
                dump($item->getOrigin()->getName().' '.$item->getCause());
            }
        }
        $dbBox = self::$manager->find(Box::class, $boxes[0]->getId());
        $this->assertEquals($dbBox->getLastFile()->getNumber(), $boxes[1]->getLastFile()->getNumber());
    }
}