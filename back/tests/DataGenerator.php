<?php


namespace App\Tests;


use App\Entity\Box;
use App\Entity\File;
use App\Entity\FileCategory;
use App\Entity\Lot;
use App\Entity\Page;
use App\Service\Entity\LotService;
use App\Service\Entity\PageService;
use Faker\Factory;
use Faker\Generator;

class DataGenerator
{

    /**
     * @var Generator
     */
    static $faker;
    private static $fileNumber = 0;

    public static function getTargetFile(): File
    {
        $lot = self::generateLot();

        return $lot->getPages()->first()->getFiles()->first();
    }

    public static function generateLot(): Lot
    {
        self::initNames();

        $lot = new Lot();
        $lot->setNumber(1);
        $lot->setDobFrom(self::$faker->dateTime());
        $lot->setDobTo(self::$faker->dateTime($lot->getDobFrom()));
        $lot->setWithAffiliation(self::$faker->boolean);
        $lot->setWithNir(self::$faker->boolean);
        $lot->setState("classification");
        LotService::addFiles($lot, self::generateFiles(400), 20);

        return $lot;
    }

    private static function initNames()
    {
        if (false === self::$faker instanceof Factory) {
            self::$faker = Factory::create('fr_FR');
        }
    }

    public static function generateFiles(int $number): array
    {
        self::initNames();

        $return = [];
        for ($i = 0; $i < $number; $i++) {
            $file = new File();
            $file->setShelf(intval($i / 150));
            $file->setNumber(-1 * self::$fileNumber++);
            $file->setDob(self::$faker->dateTime);
            $file->setFirstname(self::$faker->firstName);
            $file->setLastname(self::$faker->lastName);
            $file->setCommonName(self::$faker->lastName);
            $return[] = $file;
        }

        return $return;
    }

    public static function generatePensionLot(FileCategory $category): Lot
    {
        $lot = self::generateLot();
        $lot->setState("box_packing");
        foreach ($lot->getPages() as $page) {
            PageService::affectCategory($page, $category);
        }

        return $lot;
    }

    public static function buildCategory(): array
    {
        $cat = [
            'C',
            'DN',
            'NT',
            'P',
            'A',
        ];

        $cat = array_combine(
            $cat,
            array_map(
                function (string $code) {
                    $cat = new FileCategory();
                    $cat->setCode($code);

                    return $cat;
                },
                $cat
            )
        );

        return $cat;
    }

    public static function generateBox(Lot $lot, int $number)
    {
        $files = ($lot->getPages()->map(
            function (Page $page) {
                return $page->getFiles()->filter(
                    function (File $file) {
                        return $file->getCategory()->getCode() === 'P' && is_null($file->getBox());
                    }
                )->toArray();
            }
        )->toArray());

        $sorted = $files[0];

        usort($sorted, function (File $a, File $b) {
            if ($a->getLastname() !== $b->getLastname()) {
                return strcmp($a->getCommonName(), $b->getCommonName());
            } else {
                return strcmp($a->getFirstname(), $b->getFirstname());
            }
        });

        $box = new Box();
        $box->setNumber($number);
        $lot->addBox($box);
        $i = 0;
        foreach ($sorted as $file) {
            $box->addFile($file);
            if ($i++ === 5) {
                break;
            }
        }

        return $box;
    }


    public static function display(array $files)
    {
        foreach ($files as $file) {
            dump($file->getCommonName() . ' ' . $file->getFirstname());
        }
    }

    public static function compare(array $a, array $b)
    {
        for ($i = 0; $i < count($a); $i++) {
            $fA = $a[$i];
            $fB = $b[$i];
            if ($a[$i] !== $fB) {
                dump($fA . ' ' . str_pad($fB, 20, " ", STR_PAD_LEFT));
            }
        }
    }
}