<?php


namespace App\Tests;


use App\Service\Entity\PageService;
use PHPUnit\Framework\TestCase;

class PageServiceTest extends TestCase
{

    /**
     * @dataProvider createPagesProvider
     */
    public function testCreatePages(int $numberOfFile, int $numberByPage, int $expected)
    {
        $files = DataGenerator::generateFiles($numberOfFile);
        $pages = PageService::createPages($files, $numberByPage);
        $this->assertEquals($expected, count($pages));
    }


    public function createPagesProvider()
    {
        return [
            [0, 1, 1],
            [10, 10, 1],
            [11, 10, 2],
            [10, 1, 10],
        ];
    }
}