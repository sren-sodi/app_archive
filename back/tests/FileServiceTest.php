<?php


namespace App\Tests;


use App\Entity\Box;
use App\Entity\File;
use App\Entity\FileCategory;
use App\Repository\FileRepository;
use App\Service\Entity\FileService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FileServiceTest extends KernelTestCase
{
    /**
     * @var FileService
     */
    private $fileService;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var FileCategory[]
     */
    private $category;

    public function setUp(): void
    {
        $this->bootKernel();
        $this->manager = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        $this->fileService = self::$kernel->getContainer()->get(FileService::class);
        $this->category = array_map(
            function (FileCategory $category) {
                $this->manager->persist($category);
                $this->manager->flush();

                return $category;
            },
            DataGenerator::buildCategory()
        );
    }

    public function testFirstGetNextFileType()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $this->manager->flush();

        $file = $this->fileService->getNextFileToPack($lot);

        $this->assertTrue($file instanceof File);
    }

    public function testFirstGetNextFileCategory()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);

        $this->manager->persist($lot);
        $this->manager->flush();

        $file = $this->fileService->getNextFileToPack($lot);

        $this->assertEquals('P', $file->getCategory()->getCode());
    }

    public function testFirstGetNextFileOrder()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);

        $randPage = $lot->getPages()->get(array_rand($lot->getPages()->toArray()));
        $firstFile = $randPage->getFiles()->get(array_rand($randPage->getFiles()->toArray()));
        $firstFile->setCommonName("AAAAAA");
        $this->manager->persist($lot);
        $this->manager->flush();

        $file = $this->fileService->getNextFileToPack($lot);
        $this->assertEquals($firstFile, $file);
    }

    public function testFirstGetNextFileOrderNot()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);

        $randPage = $lot->getPages()->get(array_rand($lot->getPages()->toArray()));
        $firstFile = $randPage->getFiles()->get(array_rand($randPage->getFiles()->toArray()));
        $firstFile->setCommonName("ZZZZZZZ");
        $this->manager->persist($lot);
        $this->manager->flush();

        $file = $this->fileService->getNextFileToPack($lot);

        $this->assertNotEquals($firstFile, $file);
    }

    public function testGetFilesToPackType()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $this->manager->flush();

        $files = $this->fileService->getFilesToPack($lot);

        $retour = array_filter(array_map(function (File $file) {
            return $file->getCategory() !== $this->category['P'];
        }, $files));

        $this->assertEquals(0, count($retour));
    }

    public function testGetFilesToPackLot()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $this->manager->flush();

        $files = $this->fileService->getFilesToPack($lot);

        $retour = array_filter(array_map(function (File $file) use ($lot) {
            return $file->getPage()->getLot() !== $lot;
        }, $files));

        $this->assertEquals(0, count($retour));
    }

    public function testGetFilesToPackBox()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $this->manager->flush();

        $files = $this->fileService->getFilesToPack($lot);

        $retour = array_filter(array_map(function (File $file) use ($lot) {
            return $file->getBox() instanceof Box;
        }, $files));

        $this->assertEquals(0, count($retour));
    }

//    public function testGetFilesToPackOrder()
//    {
//        $lot = DataGenerator::generatePensionLot($this->category['P']);
//        $this->manager->persist($lot);
//        $this->manager->flush();
//
//        $files = $this->fileService->getFilesToPack($lot);
//
//        $ordered = $files;
//        FileService::sortFiles($ordered);
//        DataGenerator::compare($files, $ordered);
//
//        $this->assertTrue(array_values($ordered) === array_values($files));
//    }

    public function testEstimateRange()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $this->manager->flush();
        $files = $this->fileService->getFilesToPack($lot);
        $from = rand(0, count($files) - 1);
        $to = rand($from, count($files) - 1);
        $targetedRange = $to - $from + 1;
        $this->assertEquals($targetedRange, $this->fileService->estimateRange($files[$from], $files[$to]));
    }

    /**
     * @dataProvider stringProvider
     * @param string $firstname
     * @throws NonUniqueResultException
     */
    public function testFilteredFilesToPackFirstname(string $firstname)
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $this->manager->flush();
        $files = $this->fileService->getFilesToPack($lot);

        $filtered = array_slice(array_filter(array_map(function (File $file) use ($firstname) {
            return false === strpos(strtolower($file->getFirstname()), strtolower($firstname)) ? false : $file;
        }, $files)), 0, FileRepository::FILTER_LIMIT);
        $db = $this->fileService->getFilteredFilesToPack($lot, $firstname, "", "");

//        DataGenerator::display($filtered);
//        dump('------------');
//        DataGenerator::display($db);
        $this->assertTrue(self::getKeys($filtered) == self::getKeys($db));
    }

    private static function getKeys(array $files): array
    {
        return array_values(array_map(function (File $file) {
            return $file->getId();
        }, $files));
    }

    public function stringProvider()
    {
        return array_map(function (string $string) {
            return [$string];
        }, [
            "a",
            "em",
            "ba",
            "ze",
            "tom",
            "ez",
        ]);
    }

    public function dateProvider()
    {
        return array_map(function (string $string) {
            return [$string];
        }, [
            "2012",
            "11",
            "22",
            "2014-01",
            "-25",
        ]);
    }

    /**
     * @dataProvider stringProvider
     * @param string $commonName
     * @throws NonUniqueResultException
     */
    public function testFilteredFilesToPackCommonName(string $commonName)
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $this->manager->flush();
        $files = $this->fileService->getFilesToPack($lot);

        $filtered = array_slice(array_filter(array_map(function (File $file) use ($commonName) {
            return false === strpos(strtolower($file->getCommonName()), strtolower($commonName)) ? false : $file;
        }, $files)), 0, FileRepository::FILTER_LIMIT);
        $db = $this->fileService->getFilteredFilesToPack($lot, "", $commonName, "");
//        DataGenerator::display($filtered);
//                dump('------------');
//        DataGenerator::display($db);
        $this->assertTrue(self::getKeys($filtered) == self::getKeys($db));
    }

    /**
     * @dataProvider stringProvider
     * @param string $dob
     * @throws NonUniqueResultException
     */
    public function testFilteredFilesToPackDob(string $dob)
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $this->manager->flush();
        $files = $this->fileService->getFilesToPack($lot);

        $filtered = array_slice(array_filter(array_map(function (File $file) use ($dob) {
            return false === strpos($file->getDob()->format("Y-m-d"), $dob) ? false : $file;
        }, $files)), 0, FileRepository::FILTER_LIMIT);
        $db = $this->fileService->getFilteredFilesToPack($lot, "", "", $dob);
        $this->assertTrue(self::getKeys($filtered) == self::getKeys($db));
    }

    public function testUnboxFrom()
    {
        $lot = DataGenerator::generatePensionLot($this->category['P']);
        $this->manager->persist($lot);
        $box = DataGenerator::generateBox($lot, 0);
        $this->manager->persist($box);
        $this->manager->flush();
        $files = $box->getFiles()->toArray();
        $this->fileService->unboxFilesFromBox($box);
        $retour = true;

        /**
         * Check if the target file have been detached from the Box
         */
        foreach ($files as $file) {
            $dbFile = $this->manager->find(File::class, $file->getId());
            $retour = $retour && $dbFile->getPage()->getLot()->getId() === $lot->getId() && is_null($dbFile->getBox());
        }
        $this->assertTrue($retour);
    }
}