# Application ARCHIVE
**Gestion de la destruction des dossiers agents archivés par le SREN**

## Contenu des répertoires

Les spécifications sont dans le répertoire `/doc/Spécifications`

Le répertoire `back` contient la partie 'back office' de l'application : les services métiers et le stockage en base.
Elle est écrite en langage `php` avec l'environnement `Symfony`.

Le répertoire `front` contient la partie 'front office' de l'application : l'interface, la présentation.
Elle est écrite en langages `javascript, node.js, angular, css3, html5` et utilise la bibliothèque `primeNG`.

